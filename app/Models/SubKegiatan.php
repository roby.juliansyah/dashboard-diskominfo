<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SubKegiatan extends Model
{
    use HasFactory;
	protected $table = 'sub_kegiatan';
	protected $fillable = [
		'nama',
		'id_kegiatan'
	];
	protected $primaryKey = 'id';

    public function loadData(){
        return DB::table('sub_kegiatan')
        ->join('kegiatan', 'sub_kegiatan.id_kegiatan', '=', 'kegiatan.id')
        ->join('seksi', 'sub_kegiatan.id_seksi', '=', 'seksi.id')
        ->select('sub_kegiatan.*', 'seksi.nama as seksi', 'kegiatan.nama as kegiatan')
        ->orderBy('seksi')
        ->get();
    }

    public function loadDatabyBidang($id_bidang){
        return DB::table('sub_kegiatan')
        ->join('kegiatan', 'sub_kegiatan.id_kegiatan', '=', 'kegiatan.id')
        ->join('seksi', 'sub_kegiatan.id_seksi', '=', 'seksi.id')
        ->select('sub_kegiatan.*', 'seksi.nama as seksi', 'kegiatan.nama as kegiatan')
        ->where('seksi.id_bidang',$id_bidang)
        ->orderBy('seksi')
        ->get();
    }

    public function loadDatabySeksi($id_seksi){
        return DB::table('sub_kegiatan')
        ->join('kegiatan', 'sub_kegiatan.id_kegiatan', '=', 'kegiatan.id')
        ->join('seksi', 'sub_kegiatan.id_seksi', '=', 'seksi.id')
        ->select('sub_kegiatan.*', 'seksi.nama as seksi', 'kegiatan.nama as kegiatan')
        ->where('id_seksi',$id_seksi)
        ->orderBy('seksi')
        ->get();
    }

    public function addData($data){
        DB::table('sub_kegiatan')->insert($data);
    }

    public function deleteData($data){
        DB::table('sub_kegiatan')
            ->where('id',$data)
            ->delete($data);
    }
}
