<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MainMenu extends Model
{
    use HasFactory;

    public function loadBidang(){
        return DB::table('bidang')->get();
    }

    public function loadSeksi(){
        return DB::table('seksi')->get();
    }

    public function loadPegawai(){
        return DB::table('users')->get();
    }

    public function loadKPIKegiatan(){
        return DB::table('kegiatan_indikator')->get();
    }

    public function loadKPISubKegiatan(){
        return DB::table('sub_kegiatan_indikator')->get();
    }

    public function loadLayanan(){
        return DB::table('layanan')->get();
    }
}
