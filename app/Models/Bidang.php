<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Bidang extends Model
{
    use HasFactory;

    public function loadData(){
        return DB::table('bidang')
            ->orderBy('nama')
            ->get();
    }

    public function loadDatabyId($id_bidang){
        return DB::table('bidang')
            ->where('id',$id_bidang)
            ->orderBy('nama')
            ->get();
    }

    public function addData($data){
        DB::table('bidang')->insert($data);
    }

    public function deleteData($data){
        DB::table('bidang')
            ->where('id',$data)
            ->delete($data);
    }
}
