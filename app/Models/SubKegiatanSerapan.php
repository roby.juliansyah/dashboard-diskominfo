<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SubKegiatanSerapan extends Model
{
    use HasFactory;
    protected $table = 'sub_kegiatan_capaian_anggaran';
	protected $fillable = [
		'id_sub_kegiatan',
        'capaian_lalu',
        'capaian',
		'created_at',
	];
    protected $primaryKey = 'created_at';

    public function loadData(){
        return DB::table('sub_kegiatan_capaian_anggaran')
            ->join('sub_kegiatan', 'sub_kegiatan_capaian_anggaran.id_sub_kegiatan', '=', 'sub_kegiatan.id')
            ->join('seksi', 'sub_kegiatan.id_seksi', '=', 'seksi.id')
            ->select('sub_kegiatan_capaian_anggaran.*', 'seksi.nama as seksi', 'sub_kegiatan.nama as sub_kegiatan')
            ->get();
    }

    public function loadDataSerapanAkhirbySeksi($id_seksi){
        if($id_seksi===null){
            return DB::select('select sub_kegiatan_capaian_anggaran.id_sub_kegiatan, sub_kegiatan_capaian_anggaran.capaian, sub_kegiatan_capaian_anggaran.created_at, bidang.singkatan, seksi.id as id_seksi, seksi.nama as seksi, bidang.id as id_bidang, bidang.nama as bidang, sub_kegiatan.nama as sub_kegiatan, sub_kegiatan.anggaran as anggaran from sub_kegiatan_capaian_anggaran join sub_kegiatan on sub_kegiatan_capaian_anggaran.id_sub_kegiatan = sub_kegiatan.id join seksi on sub_kegiatan.id_seksi = seksi.id join bidang on seksi.id_bidang = bidang.id, (select id_sub_kegiatan, max(created_at) as tanggal from sub_kegiatan_capaian_anggaran group by id_sub_kegiatan) indikatorakhir where sub_kegiatan_capaian_anggaran.id_sub_kegiatan = indikatorakhir.id_sub_kegiatan and sub_kegiatan_capaian_anggaran.created_at = indikatorakhir.tanggal order by singkatan;');
        }else{
            return DB::select('select sub_kegiatan_capaian_anggaran.id_sub_kegiatan, sub_kegiatan_capaian_anggaran.capaian, sub_kegiatan_capaian_anggaran.created_at, bidang.singkatan, seksi.id as id_seksi, seksi.nama as seksi, bidang.id as id_bidang, bidang.nama as bidang, sub_kegiatan.nama as sub_kegiatan, sub_kegiatan.anggaran as anggaran from sub_kegiatan_capaian_anggaran join sub_kegiatan on sub_kegiatan_capaian_anggaran.id_sub_kegiatan = sub_kegiatan.id join seksi on sub_kegiatan.id_seksi = seksi.id join bidang on seksi.id_bidang = bidang.id, (select id_sub_kegiatan, max(created_at) as tanggal from sub_kegiatan_capaian_anggaran group by id_sub_kegiatan) indikatorakhir where sub_kegiatan_capaian_anggaran.id_sub_kegiatan = indikatorakhir.id_sub_kegiatan and sub_kegiatan_capaian_anggaran.created_at = indikatorakhir.tanggal and id_seksi = '.$id_seksi.' order by singkatan, seksi;');
        }
            
    }

    public function loadDataSerapanAkhirbyBidang($id_bidang){
        if($id_bidang===null){
            return DB::select('select sub_kegiatan_capaian_anggaran.id_sub_kegiatan, sub_kegiatan_capaian_anggaran.capaian, sub_kegiatan_capaian_anggaran.created_at, bidang.singkatan, seksi.id as id_seksi, seksi.nama as seksi, bidang.id as id_bidang, bidang.nama as bidang, sub_kegiatan.nama as sub_kegiatan, sub_kegiatan.anggaran as anggaran from sub_kegiatan_capaian_anggaran join sub_kegiatan on sub_kegiatan_capaian_anggaran.id_sub_kegiatan = sub_kegiatan.id join seksi on sub_kegiatan.id_seksi = seksi.id join bidang on seksi.id_bidang = bidang.id, (select id_sub_kegiatan, max(created_at) as tanggal from sub_kegiatan_capaian_anggaran group by id_sub_kegiatan) indikatorakhir where sub_kegiatan_capaian_anggaran.id_sub_kegiatan = indikatorakhir.id_sub_kegiatan and sub_kegiatan_capaian_anggaran.created_at = indikatorakhir.tanggal order by singkatan;');
        }else{
            return DB::select('select sub_kegiatan_capaian_anggaran.id_sub_kegiatan, sub_kegiatan_capaian_anggaran.capaian, sub_kegiatan_capaian_anggaran.created_at, bidang.singkatan, seksi.id as id_seksi, seksi.nama as seksi, bidang.id as id_bidang, bidang.nama as bidang, sub_kegiatan.nama as sub_kegiatan, sub_kegiatan.anggaran as anggaran from sub_kegiatan_capaian_anggaran join sub_kegiatan on sub_kegiatan_capaian_anggaran.id_sub_kegiatan = sub_kegiatan.id join seksi on sub_kegiatan.id_seksi = seksi.id join bidang on seksi.id_bidang = bidang.id, (select id_sub_kegiatan, max(created_at) as tanggal from sub_kegiatan_capaian_anggaran group by id_sub_kegiatan) indikatorakhir where sub_kegiatan_capaian_anggaran.id_sub_kegiatan = indikatorakhir.id_sub_kegiatan and sub_kegiatan_capaian_anggaran.created_at = indikatorakhir.tanggal and id_bidang = '.$id_bidang.' order by singkatan, seksi;');
        }
    }

    public function loadDataSerapanAkhir(){
        return DB::select('select sub_kegiatan_capaian_anggaran.id_sub_kegiatan, sub_kegiatan_capaian_anggaran.capaian, sub_kegiatan_capaian_anggaran.created_at, bidang.singkatan, seksi.id as id_seksi, seksi.nama as seksi, bidang.id as id_bidang, bidang.nama as bidang, sub_kegiatan.nama as sub_kegiatan, sub_kegiatan.anggaran as anggaran from sub_kegiatan_capaian_anggaran join sub_kegiatan on sub_kegiatan_capaian_anggaran.id_sub_kegiatan = sub_kegiatan.id join seksi on sub_kegiatan.id_seksi = seksi.id join bidang on seksi.id_bidang = bidang.id, (select id_sub_kegiatan, max(created_at) as tanggal from sub_kegiatan_capaian_anggaran group by id_sub_kegiatan) indikatorakhir where sub_kegiatan_capaian_anggaran.id_sub_kegiatan = indikatorakhir.id_sub_kegiatan and sub_kegiatan_capaian_anggaran.created_at = indikatorakhir.tanggal order by singkatan, seksi;');
    }

    public function addData($data){
        DB::table('sub_kegiatan_capaian_anggaran')->insert($data);
    }
}
