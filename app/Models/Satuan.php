<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Satuan extends Model
{
    use HasFactory;

    public function loadData(){
        return DB::table('satuan')
            ->orderBy('nama')
            ->get();
    }

    public function addData($data){
        DB::table('satuan')->insert($data);
    }

    public function deleteData($data){
        DB::table('satuan')
            ->where('id',$data)
            ->delete($data);
    }
}
