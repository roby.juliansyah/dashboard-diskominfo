<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Dashboard extends Model
{
    use HasFactory;

    public function loadAnggaranKegiatan(){
        return DB::table("kegiatan")
	    ->select(DB::raw("SUM(anggaran) as jumlahanggaran"))
	    ->get();
    }
    public function loadAnggaranSubKegiatan(){
        return DB::table("sub_kegiatan")
	    ->select(DB::raw("SUM(anggaran) as jumlahanggaran"))
	    ->get();
    }
    public function loadAnggaranKegiatanbyBidang($id_bidang){
        return DB::table("kegiatan")
	    ->select(DB::raw("SUM(anggaran) as jumlahanggaran"))
        ->where('id_bidang',$id_bidang)
	    ->get();
    }
    
    public function loadAnggaranSubKegiatanbyBidang($id_bidang){
        return DB::table("sub_kegiatan")
        ->join('seksi', 'sub_kegiatan.id_seksi', '=', 'seksi.id')
        ->join('bidang', 'seksi.id_bidang', '=', 'bidang.id')
        ->where('id_bidang',$id_bidang)
	    ->get();
    }

    public function loadJumlahAnggaranSubKegiatanbyBidang($id_bidang){
        return DB::table("sub_kegiatan")
        ->join('seksi', 'sub_kegiatan.id_seksi', '=', 'seksi.id')
        ->join('bidang', 'seksi.id_bidang', '=', 'bidang.id')
        ->where('id_bidang',$id_bidang)
	    ->get();
    }

    public function loadChartbyBidang($id_bidang){
        return DB::table("layanan")
        ->where('id_bidang',$id_bidang)
	    ->get();
    }
    
}
