<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Seksi extends Model
{
    use HasFactory;
    protected $table = 'seksi';
    protected $fillable = [
        'nama',
        'id_bidang'
    ];
    protected $primaryKey = 'id';

    public function loadData(){
        return DB::table('seksi')
            ->join('bidang', 'seksi.id_bidang', '=', 'bidang.id')
            ->select('seksi.*', 'bidang.nama as bidang')
            ->orderBy('bidang')
            ->get();
    }

    public function loadDatabyBidang($id_bidang){
        return DB::table('seksi')
            ->join('bidang', 'seksi.id_bidang', '=', 'bidang.id')
            ->select('seksi.*', 'bidang.nama as bidang')
            ->where('id_bidang',$id_bidang)
            ->orderBy('bidang')
            ->get();
    }

    public function addData($data){
        DB::table('seksi')->insert($data);
    }

    public function deleteData($data){
        
        DB::table('seksi')
            ->where('id',$data)
            ->delete($data);
        
    }
}
