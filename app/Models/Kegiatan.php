<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Kegiatan extends Model
{
    use HasFactory;
	protected $table = 'kegiatan';
	protected $fillable = [
		'nama',
		'id_bidang',
        'anggaran'
	];
	protected $primaryKey = 'id';

    public function loadData(){
        return DB::table('kegiatan')
        ->join('bidang', 'kegiatan.id_bidang', '=', 'bidang.id')
        ->select('kegiatan.*', 'bidang.nama as bidang')
        ->orderBy('bidang')
        ->get();
    }

    public function loadDatabyBidang($id_bidang){
        return DB::table('kegiatan')
        ->join('bidang', 'kegiatan.id_bidang', '=', 'bidang.id')
        ->select('kegiatan.*', 'bidang.nama as bidang')
        ->where('id_bidang',$id_bidang)
        ->orderBy('bidang')
        ->get();
    }

    public function loadDatabySeksi($id_seksi){
        return DB::table('kegiatan')
        ->join('bidang', 'kegiatan.id_bidang', '=', 'bidang.id')
        ->join('sub_kegiatan', 'sub_kegiatan.id_kegiatan', '=', 'kegiatan.id')
        ->join('seksi', 'sub_kegiatan.id_seksi', '=', 'seksi.id')
        ->select('kegiatan.*', 'bidang.nama as bidang', 'sub_kegiatan.nama as sub_kegiatan', 'seksi.id as seksi')
        ->where('id_seksi',$id_seksi)
        ->get();
    }

    public function addData($data){
        DB::table('kegiatan')->insert($data);
    }

    public function deleteData($data){
        DB::table('kegiatan')
            ->where('id',$data)
            ->delete($data);
    }
}
