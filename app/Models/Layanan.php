<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Layanan extends Model
{
    use HasFactory;
	protected $table = 'layanan';
	protected $fillable = [
		'nama',
		'id_bidang',
        'anggaran'
	];
	protected $primaryKey = 'id';

    public function loadData(){
        return DB::table('layanan')
        ->join('bidang', 'layanan.id_bidang', '=', 'bidang.id')
        ->select('layanan.*','bidang.nama as bidang')
        ->get();
    }

    public function loadDatabyBidang($id_bidang){
        return DB::table('layanan')
        ->join('bidang', 'layanan.id_bidang', '=', 'bidang.id')
        ->where('id_bidang',$id_bidang)
        ->orderBy('bidang')
        ->get();
    }

    public function addData($data){
        DB::table('layanan')->insert($data);
    }

    public function deleteData($data){
        DB::table('layanan')
            ->where('id',$data)
            ->delete($data);
    }
}
