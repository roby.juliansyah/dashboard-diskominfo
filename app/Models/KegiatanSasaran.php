<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class KegiatanSasaran extends Model
{
    use HasFactory;
    protected $table = 'kegiatan_capaian_indikator';
	protected $fillable = [
		'id_indikator_kegiatan',
        'capaian_lalu',
        'capaian',
		'created_at',
	];
    protected $primaryKey = 'created_at';

    public function loadData(){
        return DB::table('kegiatan_capaian_indikator')
            ->join('kegiatan_indikator', 'kegiatan_capaian_indikator.id_indikator_kegiatan', '=', 'kegiatan_indikator.id')
            ->join('kegiatan', 'kegiatan_indikator.id_kegiatan', '=', 'kegiatan.id')
            ->join('bidang', 'kegiatan.id_bidang', '=', 'bidang.id')
            ->select('kegiatan_capaian_indikator.*', 'bidang.id as id_bidang', 'bidang.nama as bidang', 'kegiatan.nama as kegiatan', 'kegiatan_indikator.sasaran as sasaran', 'kegiatan_indikator.target as targetsasaran')
            ->orderby('id_kegiatan','ASC')
            ->orderby('created_at','ASC')
            ->get();
    }

    public function loadDataCapaianAkhir(){
        return DB::select('select kegiatan_capaian_indikator.id_indikator_kegiatan, kegiatan_capaian_indikator.capaian, kegiatan_capaian_indikator.created_at, bidang.singkatan, bidang.id as id_bidang, kegiatan_indikator.target as targetsasaran, bidang.nama as bidang, kegiatan.nama as kegiatan, kegiatan_indikator.sasaran as sasaran, kegiatan_indikator.satuan as satuan from kegiatan_capaian_indikator join kegiatan_indikator on kegiatan_capaian_indikator.id_indikator_kegiatan = kegiatan_indikator.id join kegiatan on kegiatan_indikator.id_kegiatan = kegiatan.id join bidang on kegiatan.id_bidang = bidang.id, (select id_indikator_kegiatan, max(created_at) as tanggal from kegiatan_capaian_indikator group by id_indikator_kegiatan) indikatorakhir where kegiatan_capaian_indikator.id_indikator_kegiatan = indikatorakhir.id_indikator_kegiatan and kegiatan_capaian_indikator.created_at = indikatorakhir.tanggal order by singkatan;');
    }

    public function loadDataCapaianAkhirbyBidang($id_bidang){
        if($id_bidang===null){
            return DB::select('select kegiatan_capaian_indikator.id_indikator_kegiatan, kegiatan_capaian_indikator.capaian, kegiatan_capaian_indikator.created_at, bidang.singkatan, bidang.id as id_bidang, kegiatan_indikator.target as targetsasaran, bidang.nama as bidang, kegiatan.nama as kegiatan, kegiatan_indikator.sasaran as sasaran, kegiatan_indikator.satuan as satuan from kegiatan_capaian_indikator join kegiatan_indikator on kegiatan_capaian_indikator.id_indikator_kegiatan = kegiatan_indikator.id join kegiatan on kegiatan_indikator.id_kegiatan = kegiatan.id join bidang on kegiatan.id_bidang = bidang.id, (select id_indikator_kegiatan, max(created_at) as tanggal from kegiatan_capaian_indikator group by id_indikator_kegiatan) indikatorakhir where kegiatan_capaian_indikator.id_indikator_kegiatan = indikatorakhir.id_indikator_kegiatan and kegiatan_capaian_indikator.created_at = indikatorakhir.tanggal order by singkatan;');
        }else{
            return DB::select('select kegiatan_capaian_indikator.id_indikator_kegiatan, kegiatan_capaian_indikator.capaian, kegiatan_capaian_indikator.created_at, bidang.singkatan, bidang.id as id_bidang, kegiatan_indikator.target as targetsasaran, bidang.nama as bidang, kegiatan.nama as kegiatan, kegiatan_indikator.sasaran as sasaran, kegiatan_indikator.satuan as satuan from kegiatan_capaian_indikator join kegiatan_indikator on kegiatan_capaian_indikator.id_indikator_kegiatan = kegiatan_indikator.id join kegiatan on kegiatan_indikator.id_kegiatan = kegiatan.id join bidang on kegiatan.id_bidang = bidang.id, (select id_indikator_kegiatan, max(created_at) as tanggal from kegiatan_capaian_indikator group by id_indikator_kegiatan) indikatorakhir where kegiatan_capaian_indikator.id_indikator_kegiatan = indikatorakhir.id_indikator_kegiatan and kegiatan_capaian_indikator.created_at = indikatorakhir.tanggal and id_bidang = '.$id_bidang.' order by singkatan;');
        }
    }

    public function loadDatabyBidang($id_bidang){
        return DB::table('kegiatan_capaian_indikator')
            ->join('kegiatan_indikator', 'kegiatan_capaian_indikator.id_indikator_kegiatan', '=', 'kegiatan_indikator.id')
            ->join('kegiatan', 'kegiatan_indikator.id_kegiatan', '=', 'kegiatan.id')
            ->join('bidang', 'kegiatan.id_bidang', '=', 'bidang.id')
            ->select('kegiatan_capaian_indikator.*', 'bidang.nama as bidang', 'kegiatan.nama as kegiatan', 'kegiatan_indikator.sasaran as sasaran')
            ->where('id_bidang', $id_bidang)
            ->get();
    }

    public function addData($data){
        DB::table('kegiatan_capaian_indikator')->insert($data);
    }
}
