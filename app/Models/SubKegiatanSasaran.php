<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SubKegiatanSasaran extends Model
{
    use HasFactory;
    protected $table = 'sub_kegiatan_capaian_indikator';
	protected $fillable = [
		'id_indikator_sub_kegiatan',
        'capaian_lalu',
        'capaian',
		'created_at',
	];
    protected $primaryKey = 'created_at';

    public function loadData(){
        return DB::table('sub_kegiatan_capaian_indikator')
            ->join('sub_kegiatan_indikator', 'sub_kegiatan_capaian_indikator.id_indikator_sub_kegiatan', '=', 'sub_kegiatan_indikator.id')
            ->join('sub_kegiatan', 'sub_kegiatan_indikator.id_sub_kegiatan', '=', 'sub_kegiatan.id')
            ->join('seksi', 'sub_kegiatan.id_seksi', '=', 'seksi.id')
            ->select('sub_kegiatan_capaian_indikator.*', 'seksi.nama as seksi', 'sub_kegiatan.nama as sub_kegiatan', 'sub_kegiatan_indikator.sasaran as sasaran')
            ->get();
    }

    public function loadDataSasaranAkhir(){
        return DB::select('select sub_kegiatan_capaian_indikator.id_indikator_sub_kegiatan, sub_kegiatan_capaian_indikator.capaian, sub_kegiatan_capaian_indikator.created_at, seksi.nama as seksi, seksi.id as id_seksi, bidang.singkatan, bidang.id as id_bidang, sub_kegiatan_indikator.target as targetsasaran, bidang.nama as bidang, sub_kegiatan.nama as sub_kegiatan, sub_kegiatan_indikator.sasaran as sasaran, sub_kegiatan_indikator.satuan as satuan from sub_kegiatan_capaian_indikator join sub_kegiatan_indikator on sub_kegiatan_capaian_indikator.id_indikator_sub_kegiatan = sub_kegiatan_indikator.id join sub_kegiatan on sub_kegiatan_indikator.id_sub_kegiatan = sub_kegiatan.id join seksi on sub_kegiatan.id_seksi = seksi.id join bidang on seksi.id_bidang = bidang.id, (select id_indikator_sub_kegiatan, max(created_at) as tanggal from sub_kegiatan_capaian_indikator group by id_indikator_sub_kegiatan) indikatorakhir where sub_kegiatan_capaian_indikator.id_indikator_sub_kegiatan = indikatorakhir.id_indikator_sub_kegiatan and sub_kegiatan_capaian_indikator.created_at = indikatorakhir.tanggal order by singkatan;');
    }

    public function loadDataSasaranAkhirbyBidang($id_bidang){
        if($id_bidang===null){
            return DB::select('select sub_kegiatan_capaian_indikator.id_indikator_sub_kegiatan, sub_kegiatan_capaian_indikator.capaian, sub_kegiatan_capaian_indikator.created_at, seksi.nama as seksi, seksi.id as id_seksi, bidang.singkatan, bidang.id as id_bidang, sub_kegiatan_indikator.target as targetsasaran, bidang.nama as bidang, sub_kegiatan.nama as sub_kegiatan, sub_kegiatan_indikator.sasaran as sasaran, sub_kegiatan_indikator.satuan as satuan from sub_kegiatan_capaian_indikator join sub_kegiatan_indikator on sub_kegiatan_capaian_indikator.id_indikator_sub_kegiatan = sub_kegiatan_indikator.id join sub_kegiatan on sub_kegiatan_indikator.id_sub_kegiatan = sub_kegiatan.id join seksi on sub_kegiatan.id_seksi = seksi.id join bidang on seksi.id_bidang = bidang.id, (select id_indikator_sub_kegiatan, max(created_at) as tanggal from sub_kegiatan_capaian_indikator group by id_indikator_sub_kegiatan) indikatorakhir where sub_kegiatan_capaian_indikator.id_indikator_sub_kegiatan = indikatorakhir.id_indikator_sub_kegiatan and sub_kegiatan_capaian_indikator.created_at = indikatorakhir.tanggal order by singkatan;');
        }else{
            return DB::select('select sub_kegiatan_capaian_indikator.id_indikator_sub_kegiatan, sub_kegiatan_capaian_indikator.capaian, sub_kegiatan_capaian_indikator.created_at, seksi.nama as seksi, seksi.id as id_seksi, bidang.singkatan, bidang.id as id_bidang, sub_kegiatan_indikator.target as targetsasaran, bidang.nama as bidang, sub_kegiatan.nama as sub_kegiatan, sub_kegiatan_indikator.sasaran as sasaran, sub_kegiatan_indikator.satuan as satuan from sub_kegiatan_capaian_indikator join sub_kegiatan_indikator on sub_kegiatan_capaian_indikator.id_indikator_sub_kegiatan = sub_kegiatan_indikator.id join sub_kegiatan on sub_kegiatan_indikator.id_sub_kegiatan = sub_kegiatan.id join seksi on sub_kegiatan.id_seksi = seksi.id join bidang on seksi.id_bidang = bidang.id, (select id_indikator_sub_kegiatan, max(created_at) as tanggal from sub_kegiatan_capaian_indikator group by id_indikator_sub_kegiatan) indikatorakhir where sub_kegiatan_capaian_indikator.id_indikator_sub_kegiatan = indikatorakhir.id_indikator_sub_kegiatan and sub_kegiatan_capaian_indikator.created_at = indikatorakhir.tanggal and id_bidang = '.$id_bidang.' order by singkatan;');
        }
    }

    public function loadDataSasaranAkhirbySeksi($id_seksi){
        if($id_seksi===null){
            return DB::select('select sub_kegiatan_capaian_indikator.id_indikator_sub_kegiatan, sub_kegiatan_capaian_indikator.capaian, sub_kegiatan_capaian_indikator.created_at, seksi.nama as seksi, seksi.id as id_seksi, bidang.singkatan, bidang.id as id_bidang, sub_kegiatan_indikator.target as targetsasaran, bidang.nama as bidang, sub_kegiatan.nama as sub_kegiatan, sub_kegiatan_indikator.sasaran as sasaran, sub_kegiatan_indikator.satuan as satuan from sub_kegiatan_capaian_indikator join sub_kegiatan_indikator on sub_kegiatan_capaian_indikator.id_indikator_sub_kegiatan = sub_kegiatan_indikator.id join sub_kegiatan on sub_kegiatan_indikator.id_sub_kegiatan = sub_kegiatan.id join seksi on sub_kegiatan.id_seksi = seksi.id join bidang on seksi.id_bidang = bidang.id, (select id_indikator_sub_kegiatan, max(created_at) as tanggal from sub_kegiatan_capaian_indikator group by id_indikator_sub_kegiatan) indikatorakhir where sub_kegiatan_capaian_indikator.id_indikator_sub_kegiatan = indikatorakhir.id_indikator_sub_kegiatan and sub_kegiatan_capaian_indikator.created_at = indikatorakhir.tanggal order by singkatan;');
        }else{
            return DB::select('select sub_kegiatan_capaian_indikator.id_indikator_sub_kegiatan, sub_kegiatan_capaian_indikator.capaian, sub_kegiatan_capaian_indikator.created_at, seksi.nama as seksi, seksi.id as id_seksi, bidang.singkatan, bidang.id as id_bidang, sub_kegiatan_indikator.target as targetsasaran, bidang.nama as bidang, sub_kegiatan.nama as sub_kegiatan, sub_kegiatan_indikator.sasaran as sasaran, sub_kegiatan_indikator.satuan as satuan from sub_kegiatan_capaian_indikator join sub_kegiatan_indikator on sub_kegiatan_capaian_indikator.id_indikator_sub_kegiatan = sub_kegiatan_indikator.id join sub_kegiatan on sub_kegiatan_indikator.id_sub_kegiatan = sub_kegiatan.id join seksi on sub_kegiatan.id_seksi = seksi.id join bidang on seksi.id_bidang = bidang.id, (select id_indikator_sub_kegiatan, max(created_at) as tanggal from sub_kegiatan_capaian_indikator group by id_indikator_sub_kegiatan) indikatorakhir where sub_kegiatan_capaian_indikator.id_indikator_sub_kegiatan = indikatorakhir.id_indikator_sub_kegiatan and sub_kegiatan_capaian_indikator.created_at = indikatorakhir.tanggal and id_seksi = '.$id_seksi.' order by singkatan;');
        }
    }

    public function loadDatabyBidang($id_bidang){
        return DB::table('kegiatan_capaian_indikator')
            ->join('kegiatan_indikator', 'kegiatan_capaian_indikator.id_indikator_kegiatan', '=', 'kegiatan_indikator.id')
            ->join('kegiatan', 'kegiatan_indikator.id_kegiatan', '=', 'kegiatan.id')
            ->join('bidang', 'kegiatan.id_bidang', '=', 'bidang.id')
            ->select('kegiatan_capaian_indikator.*', 'bidang.nama as bidang', 'kegiatan.nama as kegiatan', 'kegiatan_indikator.sasaran as sasaran')
            ->where('id_bidang', $id_bidang)
            ->get();
    }

    public function addData($data){
        DB::table('sub_kegiatan_capaian_indikator')->insert($data);
    }
}
