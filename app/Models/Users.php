<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Users extends Model
{
    use HasFactory;

    public function loadData(){
        return DB::table('users')
        ->join('bidang', 'users.id_bidang', '=', 'bidang.id', 'left outer')
        ->join('seksi', 'users.id_seksi', '=', 'seksi.id', 'left outer')
        ->select('users.*', 'bidang.nama as bidang', 'bidang.keterangan as bidangket', 'seksi.nama as seksi', 'seksi.keterangan as seksiket')
        ->orderby('name')
        ->get();
    }

    public function addData($data){
        DB::table('users')->insert($data);
    }

    public function deleteData($data){
        DB::table('users')
            ->where('id',$data)
            ->delete($data);
    }
}
