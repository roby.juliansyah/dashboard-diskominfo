<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class KegiatanIndikator extends Model
{
    use HasFactory;
	protected $table = 'kegiatan_indikator';
	protected $fillable = [
		'sasaran',
		'id_kegiatan',
        'target',
	];
	protected $primaryKey = 'id';

    public function loadData(){
        return DB::table('kegiatan_indikator')
        ->join('kegiatan', 'kegiatan_indikator.id_kegiatan', '=', 'kegiatan.id')
        ->join('bidang', 'kegiatan.id_bidang', '=', 'bidang.id')
        ->select('kegiatan_indikator.*', 'kegiatan.nama as kegiatan', 'bidang.nama as bidang')
        ->orderBy('bidang')
        ->get();
    }

    public function loadDatabyBidang($id_bidang){
        return DB::table('kegiatan_indikator')
        ->join('kegiatan', 'kegiatan_indikator.id_kegiatan', '=', 'kegiatan.id')
        ->join('bidang', 'kegiatan.id_bidang', '=', 'bidang.id')
        ->select('kegiatan_indikator.*', 'kegiatan.nama as kegiatan', 'bidang.nama as bidang')
        ->where('id_bidang',$id_bidang)
        ->orderBy('bidang')
        ->get();
    }
    
    public function addData($data){
        DB::table('kegiatan_indikator')->insert($data);
    }

    public function deleteData($data){
        DB::table('kegiatan_indikator')
            ->where('id',$data)
            ->delete($data);
    }
}
