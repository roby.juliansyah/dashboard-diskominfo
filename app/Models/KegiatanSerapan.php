<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class KegiatanSerapan extends Model
{
    use HasFactory;
    protected $table = 'kegiatan_capaian_anggaran';
	protected $fillable = [
		'id_kegiatan',
        'capaian_lalu',
        'capaian',
		'created_at',
	];
    protected $primaryKey = 'created_at';

    public function loadData(){
        return DB::table('kegiatan_capaian_anggaran')
            ->join('kegiatan', 'kegiatan_capaian_anggaran.id_kegiatan', '=', 'kegiatan.id')
            ->join('bidang', 'kegiatan.id_bidang', '=', 'bidang.id')
            ->select('kegiatan_capaian_anggaran.*', 'bidang.nama as bidang', 'kegiatan.nama as kegiatan')
            ->get();
    }

    public function loadDataSerapanAkhirbyBidang($id_bidang){
        if($id_bidang===null){
            return DB::select('select kegiatan_capaian_anggaran.id_kegiatan, kegiatan_capaian_anggaran.capaian, kegiatan_capaian_anggaran.created_at, bidang.singkatan, bidang.id as id_bidang, bidang.nama as bidang, kegiatan.nama as kegiatan, kegiatan.anggaran as anggaran from kegiatan_capaian_anggaran join kegiatan on kegiatan_capaian_anggaran.id_kegiatan = kegiatan.id join bidang on kegiatan.id_bidang = bidang.id, (select id_kegiatan, max(created_at) as tanggal from kegiatan_capaian_anggaran group by id_kegiatan) indikatorakhir where kegiatan_capaian_anggaran.id_kegiatan = indikatorakhir.id_kegiatan and kegiatan_capaian_anggaran.created_at = indikatorakhir.tanggal order by singkatan;');
        }else{
            return DB::select('select kegiatan_capaian_anggaran.id_kegiatan, kegiatan_capaian_anggaran.capaian, kegiatan_capaian_anggaran.created_at, bidang.singkatan, bidang.id as id_bidang, bidang.nama as bidang, kegiatan.nama as kegiatan, kegiatan.anggaran as anggaran from kegiatan_capaian_anggaran join kegiatan on kegiatan_capaian_anggaran.id_kegiatan = kegiatan.id join bidang on kegiatan.id_bidang = bidang.id, (select id_kegiatan, max(created_at) as tanggal from kegiatan_capaian_anggaran group by id_kegiatan) indikatorakhir where kegiatan_capaian_anggaran.id_kegiatan = indikatorakhir.id_kegiatan and kegiatan_capaian_anggaran.created_at = indikatorakhir.tanggal and id_bidang = '.$id_bidang.' order by singkatan;');
        }
            
    }

    public function loadDataSerapanAkhir(){
        return DB::select('select kegiatan_capaian_anggaran.id_kegiatan, kegiatan_capaian_anggaran.capaian, kegiatan_capaian_anggaran.created_at, bidang.singkatan, bidang.id as id_bidang, bidang.nama as bidang, kegiatan.nama as kegiatan, kegiatan.anggaran as anggaran  from kegiatan_capaian_anggaran join kegiatan on kegiatan_capaian_anggaran.id_kegiatan = kegiatan.id join bidang on kegiatan.id_bidang = bidang.id, (select id_kegiatan, max(created_at) as tanggal from kegiatan_capaian_anggaran group by id_kegiatan) indikatorakhir where kegiatan_capaian_anggaran.id_kegiatan = indikatorakhir.id_kegiatan and kegiatan_capaian_anggaran.created_at = indikatorakhir.tanggal order by singkatan;');
    }

    public function addData($data){
        DB::table('kegiatan_capaian_anggaran')->insert($data);
    }
}
