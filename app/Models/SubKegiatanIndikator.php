<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SubKegiatanIndikator extends Model
{
    use HasFactory;
	protected $table = 'sub_kegiatan_indikator';
	protected $fillable = [
		'sasaran',
		'id_sub_kegiatan',
        'target'
	];
	protected $primaryKey = 'id';

    public function loadData(){
        return DB::table('sub_kegiatan_indikator')
        ->join('sub_kegiatan', 'sub_kegiatan_indikator.id_sub_kegiatan', '=', 'sub_kegiatan.id')
        ->join('seksi', 'sub_kegiatan.id_seksi', '=', 'seksi.id')
        ->select('sub_kegiatan_indikator.*', 'sub_kegiatan.nama as sub_kegiatan', 'seksi.nama as seksi')
        ->orderBy('seksi')
        ->get();
    }

    public function loadDatabyBidang($id_bidang){
        return DB::table('sub_kegiatan_indikator')
        ->join('sub_kegiatan', 'sub_kegiatan_indikator.id_sub_kegiatan', '=', 'sub_kegiatan.id')
        ->join('seksi', 'sub_kegiatan.id_seksi', '=', 'seksi.id')
        ->select('sub_kegiatan_indikator.*', 'sub_kegiatan.nama as sub_kegiatan', 'seksi.nama as seksi')
        ->where('seksi.id_bidang',$id_bidang)
        ->orderBy('seksi')
        ->get();
    }

    public function loadDatabySeksi($id_seksi){
        return DB::table('sub_kegiatan_indikator')
        ->join('sub_kegiatan', 'sub_kegiatan_indikator.id_sub_kegiatan', '=', 'sub_kegiatan.id')
        ->join('seksi', 'sub_kegiatan.id_seksi', '=', 'seksi.id')
        ->select('sub_kegiatan_indikator.*', 'sub_kegiatan.nama as sub_kegiatan', 'seksi.nama as seksi')
        ->where('id_seksi',$id_seksi)
        ->orderBy('seksi')
        ->get();
    }

    public function addData($data){
        DB::table('sub_kegiatan_indikator')->insert($data);
    }

    public function deleteData($data){
        DB::table('sub_kegiatan_indikator')
            ->where('id',$data)
            ->delete($data);
    }
}
