<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Seksi;
use App\Models\Bidang;
use App\Models\Dashboard;
use App\Models\Layanan;
use App\Models\SubKegiatanSasaran;
use App\Models\SubKegiatanSerapan;
use App\Models\SubKegiatanFisik;
use Illuminate\Support\Facades\Auth;

class BidangController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->Seksi = new Seksi();
        $this->Bidang = new Bidang();
        $this->Dashboard = new Dashboard();
        $this->Layanan = new Layanan();
        $this->SubKegiatanSasaran = new SubKegiatanSasaran();
        $this->SubKegiatanSerapan = new SubKegiatanSerapan();
        $this->SubKegiatanFisik = new SubKegiatanFisik();
    }
    
    public function detailbidang($idbidang){
        if (Auth::user()->id_bidang === (int)$idbidang || Auth::user()->id_bidang === null){
            $data = [   
                'seksibybidang' => $this->Seksi->loadDatabyBidang($idbidang),
                'anggarankegiatanbybidang' => $this->Dashboard->loadAnggaranKegiatanbyBidang($idbidang),
                'anggaransubkegiatanbybidang' => $this->Dashboard->loadAnggaranSubKegiatanbyBidang($idbidang),
                'datasubkegiatansasaran' => $this->SubKegiatanSasaran->loadDataSasaranAkhirbyBidang($idbidang),
                'datasubkegiatanserapan' => $this->SubKegiatanSerapan->loadDataSerapanAkhirbyBidang($idbidang),
                'datasubkegiatanfisik' => $this->SubKegiatanFisik->loadDataFisikAkhirbyBidang($idbidang),
                'chartbybidang' => $this->Dashboard->loadChartbyBidang($idbidang),
                'bidang' => $this->Bidang->loadData(),
                'bidangbyid' => $this->Bidang->loadDatabyId($idbidang)
            ];
            return view('bidang.detailbidang', $data);
        }else{
            return redirect('/bidang/'.Auth::user()->id_bidang);
        }
        
    }
}
