<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Bidang;
use App\Models\Seksi;
use App\Models\Dashboard;
use App\Models\KegiatanSasaran;
use App\Models\SubKegiatanSasaran;
use App\Models\KegiatanSerapan;
use App\Models\SubKegiatanSerapan;
use App\Models\KegiatanFisik;
use App\Models\SubKegiatanFisik;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->Bidang = new Bidang();
        $this->Seksi = new Seksi();
        $this->Dashboard = new Dashboard();
        $this->KegiatanSasaran = new KegiatanSasaran();
        $this->SubKegiatanSasaran = new SubKegiatanSasaran();
        $this->KegiatanSerapan = new KegiatanSerapan();
        $this->SubKegiatanSerapan = new SubKegiatanSerapan();
        $this->KegiatanFisik = new KegiatanFisik();
        $this->SubKegiatanFisik = new SubKegiatanFisik();
    }
    
    public function index(){
        if (Auth::user()->id_bidang>0){
            return redirect('/bidang/'.Auth::user()->id_bidang);
        }else{
            $data = [
                'datakegiatansasaran' => $this->KegiatanSasaran->loadDataCapaianAkhir(),
                'datasubkegiatansasaran' => $this->SubKegiatanSasaran->loadDataSasaranAkhir(),
                'anggarankegiatan' => $this->Dashboard->loadAnggaranKegiatan(),
                'anggaransubkegiatan' => $this->Dashboard->loadAnggaranSubKegiatan(),
                'datakegiatanserapan' => $this->KegiatanSerapan->loadDataSerapanAkhir(),
                'datasubkegiatanserapan' => $this->SubKegiatanSerapan->loadDataSerapanAkhir(),
                'datakegiatanfisik' => $this->KegiatanFisik->loadDataFisikAkhir(),
                'datasubkegiatanfisik' => $this->SubKegiatanFisik->loadDataFisikAkhir(),
                'bidang' => $this->Bidang->loadData(),
                'seksi' => $this->Seksi->loadData(),
                'seksibybidang' => $this->Seksi->loadDatabyBidang(Auth::user()->id_seksi),
            ];
            return view('welcome', $data);
        }
        
    }
}
