<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\MainMenu;
use App\Models\Kegiatan;
use App\Models\KegiatanIndikator;
use App\Models\SubKegiatan;
use App\Models\SubKegiatanIndikator;
use App\Models\KegiatanSasaran;
use App\Models\KegiatanSerapan;
use App\Models\KegiatanFisik;
use App\Models\SubKegiatanSasaran;
use App\Models\SubKegiatanSerapan;
use App\Models\SubKegiatanFisik;
use App\Models\Layanan;
use App\Models\Bidang;
use App\Models\Seksi;
use App\Models\Users;
use App\Models\Satuan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class BackendController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->MainMenu = new MainMenu();
        $this->Kegiatan = new Kegiatan();
        $this->KegiatanIndikator = new KegiatanIndikator();
        $this->SubKegiatan = new SubKegiatan();
        $this->SubKegiatanIndikator = new SubKegiatanIndikator();
        $this->KegiatanSasaran = new KegiatanSasaran();
        $this->KegiatanSerapan = new KegiatanSerapan();
        $this->KegiatanFisik = new KegiatanFisik();
        $this->SubKegiatanSasaran = new SubKegiatanSasaran();
        $this->SubKegiatanSerapan = new SubKegiatanSerapan();
        $this->SubKegiatanFisik = new SubKegiatanFisik();
        $this->Layanan = new Layanan();
        $this->Bidang = new Bidang();
        $this->Seksi = new Seksi();
        $this->Users = new Users();
        $this->Satuan = new Satuan();
    }
    
    public function mainmenu(){
        $data = [
            'bidang' => $this->MainMenu->loadBidang(),
            'seksi' => $this->MainMenu->loadSeksi(),
            'pegawai' => $this->MainMenu->loadPegawai(),
            'kpikegiatan' => $this->MainMenu->loadKPIKegiatan(),
            'kpisubkegiatan' => $this->MainMenu->loadKPISubKegiatan(),
            'layanan' => $this->MainMenu->loadLayanan(),
        ];
        return view('backend.mainmenu', $data);
    }

//--- DATA USER ------------------------------------------------------------------------------------------------------------------------------------------

    public function userman(){
        if (Auth::user()->id_seksi>0){
            return redirect('/admin/mainmenu');
        }else{
            return view('backend.userman');
        }
    }

//--- DATA KEGIATAN ------------------------------------------------------------------------------------------------------------------------------------------

    public function dpakegiatan(){
        if (Auth::user()->id_seksi>0){
            return redirect('/admin/mainmenu');
        }else{
            $tahun = date('Y', strtotime(now()));
            $data = [
                        'kegiatan' => $this->Kegiatan->loadData(),
                        'bidang' => $this->Bidang->loadData(),
                        'kegiatanbybidang' => $this->Kegiatan->loadDatabyBidang(Auth::user()->id_bidang),
                        'tahun' => $tahun
                    ];
            return view('backend.dpakegiatan', $data);
        }
    }

    public function dpakegiatanadd(){
        Request()->validate([
            'nama_kegiatan' => 'required|unique:kegiatan,nama|max:150',
            'nama_bidang' => 'required',
            'kodering_kegiatan' => 'required',
            'anggaran_kegiatan' => 'required',
        ],[
            'nama_kegiatan.required' => 'Nama Kegiatan harus diisi',
            'nama_kegiatan.unique' => 'Nama Kegiatan sudah ada',
            'nama_kegiatan.max' => 'Maksimal 150 karakter',
            'nama_bidang.required' => 'Nama Bidang harus dipilih',
            'kodering_kegiatan.required' => 'Kode Rekening harus diisi',
            'anggaran_kegiatan.required' => 'Anggaran Kegiatan harus diisi',
        ]);
        $charlist = ["Rp", ".", " "];
        $anggaran = str_replace ( "Rp. ", "",Request()->anggaran_kegiatan);
        $anggaran = str_replace ( ".", "",$anggaran);

        $data = [
            'id_bidang' => Request()->nama_bidang,
            'kode' => Request()->kodering_kegiatan,
            'nama' => Request()->nama_kegiatan,
            'anggaran' => $anggaran,
            'capaian_anggaran' => 0,
            'capaian_fisik' => 0,
            'tahun' => Request()->tahun_kegiatan,
            'created_at' => now(),
            'created_by' => Auth::user()->id,
            'updated_at' => now(),
            'updated_by' => Auth::user()->id,
        ];

        $this->Kegiatan->addData($data);
        return redirect()->route('dpakegiatan')->with('pesan','data berhasil ditambahkan');
    }

    public function dpakegiatandelete($idkegiatan){
        if (Auth::user()->id_seksi>0){
            return redirect('/admin/mainmenu');
        }else{
            $this->Kegiatan->deleteData($idkegiatan);
            return redirect()->route('dpakegiatan')->with('pesan','data berhasil dihapus');
        }
    }

//--- DATA KEGIATAN INDIKATOR ------------------------------------------------------------------------------------------------------------------------------------------

    public function dpakegiatanindikator(){
        if (Auth::user()->id_seksi>0){
            return redirect('/admin/mainmenu');
        }else{
            $data = [
                    'kegiatanindikator' => $this->KegiatanIndikator->loadData(),
                    'bidang' => $this->Bidang->loadData(),
                    'kegiatan' => $this->Kegiatan->loadData(),
                    'kegiatanbybidang' => $this->Kegiatan->loadDatabyBidang(Auth::user()->id_bidang),
                    'kegiatanindikatorbybidang' => $this->KegiatanIndikator->loadDatabyBidang(Auth::user()->id_bidang),
                    
                ];
            return view('backend.dpakegiatanindikator', $data);
        }
    }

    public function dpakegiatanindikatoradd(){
        Request()->validate([
            'sasaran_kegiatan' => 'required|unique:kegiatan_indikator,sasaran|max:150',
            'nama_kegiatan' => 'required',
            'indikator_kegiatan' => 'required',
            'target_kegiatan' => 'required',
            'satuan_target' => 'required',
        ],[
            'sasaran_kegiatan.required' => 'Sasaran Kegiatan harus diisi',
            'sasaran_kegiatan.unique' => 'Sasaran Kegiatan sudah ada',
            'sasaran_kegiatan.max' => 'Maksimal 150 karakter',
            'nama_kegiatan.required' => 'Nama Kegiatan harus dipilih',
            'indikator_kegiatan.required' => 'Indikator Kegiatan harus diisi',
            'target_kegiatan.required' => 'Target Kegiatan harus diisi',
            'satuan_target.required' => 'Satuan Target harus diisi',
        ]);

        $data = [
            'id_kegiatan' => Request()->nama_kegiatan,
            'sasaran' => Request()->sasaran_kegiatan,
            'indikator' => Request()->indikator_kegiatan,
            'target' => Request()->target_kegiatan,
            'capaian' => 0,
            'satuan' => Request()->satuan_target,
            'updated_at' => now(),
            'updated_by' => Auth::user()->id,
        ];

        $this->KegiatanIndikator->addData($data);
        return redirect()->route('dpakegiatanindikator')->with('pesan','data berhasil ditambahkan');
    }

    public function dpakegiatanindikatordelete($idkegiatan){
        if (Auth::user()->id_seksi>0){
            return redirect('/admin/mainmenu');
        }else{
            $this->KegiatanIndikator->deleteData($idkegiatan);
            return redirect()->route('dpakegiatanindikator')->with('pesan','data berhasil dihapus');
        }
    }

//--- DATA SUB KEGIATAN ------------------------------------------------------------------------------------------------------------------------------------------

    public function dpasubkegiatan(){
        $data = [
                    'subkegiatan' => $this->SubKegiatan->loadData(),
                    'kegiatan' => $this->Kegiatan->loadData(),
                    'bidang' => $this->Bidang->loadData(),
                    'seksi' => $this->Seksi->loadData(),
                    'kegiatanbybidang' => $this->Kegiatan->loadDatabyBidang(Auth::user()->id_bidang),
                    'seksibybidang' => $this->Seksi->loadDatabyBidang(Auth::user()->id_bidang),
                    'subkegiatanbybidang' => $this->SubKegiatan->loadDatabyBidang(Auth::user()->id_bidang),
                    'subkegiatanbyseksi' => $this->SubKegiatan->loadDatabySeksi(Auth::user()->id_seksi),
                ];
        return view('backend.dpasubkegiatan', $data);
    }

    public function dpasubkegiatanadd(){
        Request()->validate([
            'nama_sub_kegiatan' => 'required|unique:kegiatan,nama|max:150',
            'nama_kegiatan' => 'required',
            'nama_seksi' => 'required',
            'kodering_sub_kegiatan' => 'required',
            'anggaran_sub_kegiatan' => 'required',
        ],[
            'nama_sub_kegiatan.required' => 'Nama Sub Kegiatan harus diisi',
            'nama_sub_kegiatan.unique' => 'Nama Sub Kegiatan sudah ada',
            'nama_sub_kegiatan.max' => 'Maksimal 150 karakter',
            'nama_kegiatan.required' => 'Nama Kegiatan harus dipilih',
            'nama_seksi.required' => 'Nama Seksi harus dipilih',
            'kodering_sub_kegiatan.required' => 'Kode Rekening harus diisi',
            'anggaran_sub_kegiatan.required' => 'Anggaran Sub Kegiatan harus diisi',
        ]);

        $data = [
            'id_kegiatan' => Request()->nama_kegiatan,
            'id_seksi' => Request()->nama_seksi,
            'kode' => Request()->kodering_sub_kegiatan,
            'nama' => Request()->nama_sub_kegiatan,
            'anggaran' => preg_replace(array('/\s+/','/R/','/p/','/\./'), array('','','',''), Request()->anggaran_sub_kegiatan),
            'capaian_anggaran' => 0,
            'capaian_fisik' => 0,
            'created_at' => now(),
            'created_by' => Auth::user()->id,
            'updated_at' => now(),
            'updated_by' => Auth::user()->id,
        ];

        $this->SubKegiatan->addData($data);
        return redirect()->route('dpasubkegiatan')->with('pesan','data berhasil ditambahkan');
    }

    public function dpasubkegiatandelete($idsubkegiatan){
        $this->SubKegiatan->deleteData($idsubkegiatan);
        return redirect()->route('dpasubkegiatan')->with('pesan','data berhasil dihapus');
    }

//--- DATA SUB KEGIATAN INDIKATOR ------------------------------------------------------------------------------------------------------------------------------------------

    public function dpasubkegiatanindikator(){
        $tahun = date('Y', strtotime(now()));
        $data = [
            'subkegiatanindikator' => $this->SubKegiatanIndikator->loadData(),
            'subkegiatan' => $this->SubKegiatan->loadData(),
            'kegiatan' => $this->Kegiatan->loadData(),
            'bidang' => $this->Bidang->loadData(),
            'seksi' => $this->Seksi->loadData(),
            'kegiatanbybidang' => $this->Kegiatan->loadDatabyBidang(Auth::user()->id_bidang),
            'kegiatanbyseksi' => $this->Kegiatan->loadDatabySeksi(Auth::user()->id_seksi),
            'seksibybidang' => $this->Seksi->loadDatabyBidang(Auth::user()->id_bidang),
            'subkegiatanbyseksi' => $this->SubKegiatan->loadDatabySeksi(Auth::user()->id_seksi),
            'subkegiatanindikatorbybidang' => $this->SubKegiatanIndikator->loadDatabyBidang(Auth::user()->id_bidang),
            'subkegiatanindikatorbyseksi' => $this->SubKegiatanIndikator->loadDatabySeksi(Auth::user()->id_seksi),    
        ];
        return view('backend.dpasubkegiatanindikator', $data);
    }

    public function dpasubkegiatanindikatoradd(){
        Request()->validate([
            'sasaran_sub_kegiatan' => 'required|unique:sub_kegiatan_capaian_indikator,sasaran|max:150',
            'nama_sub_kegiatan' => 'required',
            'indikator_sub_kegiatan' => 'required',
            'target_sub_kegiatan' => 'required',
            'satuan_target' => 'required',
        ],[
            'sasaran_sub_kegiatan.required' => 'Sasaran Sub Kegiatan harus diisi',
            'sasaran_sub_kegiatan.unique' => 'Sasaran Sub Kegiatan sudah ada',
            'sasaran_sub_kegiatan.max' => 'Maksimal 150 karakter',
            'nama_sub_kegiatan.required' => 'Nama Sub Kegiatan harus dipilih',
            'indikator_sub_kegiatan.required' => 'Indikator Sub Kegiatan harus diisi',
            'target_sub_kegiatan.required' => 'Target Sub Kegiatan harus diisi',
            'satuan_target.required' => 'Satuan Target harus diisi',
        ]);

        $data = [
            'id_sub_kegiatan' => Request()->nama_sub_kegiatan,
            'sasaran' => Request()->sasaran_sub_kegiatan,
            'indikator' => Request()->indikator_sub_kegiatan,
            'target' => Request()->target_sub_kegiatan,
            'capaian' => 0,
            'satuan' => Request()->satuan_target,
            'updated_at' => now(),
            'updated_by' => Auth::user()->id,
        ];

        $this->SubKegiatanIndikator->addData($data);
        return redirect()->route('dpasubkegiatanindikator')->with('pesan','data berhasil ditambahkan');
    }

    public function dpasubkegiatanindikatordelete($idsubkegiatanindikator){
        $this->SubKegiatanIndikator->deleteData($idsubkegiatanindikator);
        return redirect()->route('dpasubkegiatanindikator')->with('pesan','data berhasil dihapus');
    }

//--- DATA KEGIATAN SASARAN ------------------------------------------------------------------------------------------------------------------------------------------

    public function datakegiatansasaran(){
        if (Auth::user()->id_seksi>0){
            return redirect('/admin/mainmenu');
        }else{
            $data = [
                'datakegiatansasaran' => $this->KegiatanSasaran->loadDataCapaianAkhir(),
                'datakegiatansasaranbybidang' => $this->KegiatanSasaran->loadDataCapaianAkhirbyBidang(Auth::user()->id_bidang),
                'kegiatan' => $this->Kegiatan->loadData(),
                'bidang' => $this->Bidang->loadData(),
                'kegiatanindikator' => $this->KegiatanIndikator->loadData(),
                'kegiatanbybidang' => $this->Kegiatan->loadDatabyBidang(Auth::user()->id_bidang),
            ];
            return view('backend.datakegiatansasaran', $data);
        }
    }

    public function datakegiatansasaranadd(){
        Request()->validate([
            'capaian_saat_ini' => 'required|max:150',
            'kegiatan' => 'required',
            'kegiatan_indikator' => 'required',
        ],[
            'capaian_saat_ini.required' => 'Sasaran Kegiatan harus diisi',
            'capaian_saat_ini.max' => 'Maksimal 150 karakter',
            'kegiatan.required' => 'Kegiatan harus dipilih',
            'kegiatan_indikator.required' => 'Kegiatan harus dipilih',
        ]);

        $data = [
            'id_indikator_kegiatan' => Request()->kegiatan_indikator,
            'capaian_lalu' => Request()->capaian_lalu,
            'capaian' => Request()->capaian_saat_ini,
            'created_at' => now(),
            'created_by' => Auth::user()->id,
        ];

        $this->KegiatanSasaran->addData($data);
        return redirect()->route('datakegiatansasaran')->with('pesan','data berhasil ditambahkan');
    }

//--- DATA KEGIATAN SERAPAN ------------------------------------------------------------------------------------------------------------------------------------------

    public function datakegiatanserapan(){
        if (Auth::user()->id_seksi>0){
            return redirect('/admin/mainmenu');
        }else{
            $data = [
                'datakegiatanserapan' => $this->KegiatanSerapan->loadDataSerapanAkhir(),
                'datakegiatanserapanbybidang' => $this->KegiatanSerapan->loadDataSerapanAkhirbyBidang(Auth::user()->id_bidang),
                'kegiatan' => $this->Kegiatan->loadData(),
                'bidang' => $this->Bidang->loadData(),
                'kegiatanindikator' => $this->KegiatanIndikator->loadData(),
                'kegiatanbybidang' => $this->Kegiatan->loadDatabyBidang(Auth::user()->id_bidang),
            ];
            return view('backend.datakegiatanserapan', $data);
        }
    }

    public function datakegiatanserapanadd(){
        Request()->validate([
            'capaian_saat_ini' => 'required|max:150',
            'kegiatan' => 'required',
        ],[
            'capaian_saat_ini.required' => 'Sasaran Kegiatan harus diisi',
            'capaian_saat_ini.max' => 'Maksimal 150 karakter',
            'kegiatan.required' => 'Kegiatan harus dipilih',
        ]);

        $data = [
            'id_kegiatan' => Request()->kegiatan,
            'capaian_lalu' => preg_replace(array('/\s+/','/R/','/p/','/\./'), array('','','',''), Request()->capaian_lalu),
            'capaian' => preg_replace(array('/\s+/','/R/','/p/','/\./'), array('','','',''), Request()->capaian_saat_ini),
            'created_at' => now(),
            'created_by' => Auth::user()->id,
        ];

        $this->KegiatanSerapan->addData($data);
        return redirect()->route('datakegiatanserapan')->with('pesan','data berhasil ditambahkan');
    }

//--- DATA KEGIATAN FISIK ------------------------------------------------------------------------------------------------------------------------------------------

    public function datakegiatanfisik(){
        if (Auth::user()->id_seksi>0){
            return redirect('/admin/mainmenu');
        }else{
            $data = [
                'datakegiatanfisik' => $this->KegiatanFisik->loadDataFisikAkhir(),
                'datakegiatanfisikbybidang' => $this->KegiatanFisik->loadDataFisikAkhirbyBidang(Auth::user()->id_bidang),
                'kegiatan' => $this->Kegiatan->loadData(),
                'bidang' => $this->Bidang->loadData(),
                'kegiatanindikator' => $this->KegiatanIndikator->loadData(),
                'kegiatanbybidang' => $this->Kegiatan->loadDatabyBidang(Auth::user()->id_bidang),
            ];
            return view('backend.datakegiatanfisik', $data);
        }
    }

    public function datakegiatanfisikadd(){
        Request()->validate([
            'capaian_saat_ini' => 'required|max:100',
            'kegiatan' => 'required',
        ],[
            'capaian_saat_ini.required' => 'Sasaran Kegiatan harus diisi',
            'capaian_saat_ini.max' => 'Maksimal 100 Persen',
            'kegiatan.required' => 'Kegiatan harus dipilih',
        ]);

        $data = [
            'id_kegiatan' => Request()->kegiatan,
            'capaian_lalu' => Request()->capaian_lalu,
            'capaian' => Request()->capaian_saat_ini,
            'created_at' => now(),
            'created_by' => Auth::user()->id,
        ];

        $this->KegiatanFisik->addData($data);
        return redirect()->route('datakegiatanfisik')->with('pesan','data berhasil ditambahkan');
    }

//--- DATA SUB KEGIATAN SASARAN ------------------------------------------------------------------------------------------------------------------------------------------

    public function datasubkegiatansasaran(){
        $data = [
            'datasubkegiatansasaran' => $this->SubKegiatanSasaran->loadDataSasaranAkhir(),
            'datasubkegiatansasaranbybidang' => $this->SubKegiatanSasaran->loadDataSasaranAkhirbyBidang(Auth::user()->id_bidang),
            'datasubkegiatansasaranbyseksi' => $this->SubKegiatanSasaran->loadDataSasaranAkhirbySeksi(Auth::user()->id_seksi),
            'kegiatan' => $this->Kegiatan->loadData(),
            'subkegiatan' => $this->SubKegiatan->loadData(),
            'bidang' => $this->Bidang->loadData(),
            'subkegiatanindikator' => $this->SubKegiatanIndikator->loadData(),
            'kegiatanbybidang' => $this->Kegiatan->loadDatabyBidang(Auth::user()->id_bidang),
            'kegiatanbyseksi' => $this->Kegiatan->loadDatabySeksi(Auth::user()->id_seksi),
        ];
        return view('backend.datasubkegiatansasaran', $data);
    }

    public function datasubkegiatansasaranadd(){
        Request()->validate([
            'capaian_saat_ini' => 'required|max:150',
            'kegiatan' => 'required',
            'sub_kegiatan' => 'required',
            'sub_kegiatan_indikator' => 'required',
        ],[
            'capaian_saat_ini.required' => 'Sasaran Sub Kegiatan harus diisi',
            'capaian_saat_ini.max' => 'Maksimal 150 karakter',
            'kegiatan.required' => 'Kegiatan harus dipilih',
            'sub_kegiatan.required' => 'Sub Kegiatan harus dipilih',
            'sub_kegiatan_indikator.required' => 'Sub Kegiatan harus dipilih',
        ]);

        $data = [
            'id_indikator_sub_kegiatan' => Request()->sub_kegiatan_indikator,
            'capaian_lalu' => Request()->capaian_lalu,
            'capaian' => Request()->capaian_saat_ini,
            'created_at' => now(),
            'created_by' => Auth::user()->id,
        ];

        $this->SubKegiatanSasaran->addData($data);
        return redirect()->route('datasubkegiatansasaran')->with('pesan','data berhasil ditambahkan');
    }

//--- DATA SUB KEGIATAN SERAPAN ------------------------------------------------------------------------------------------------------------------------------------------

    public function datasubkegiatanserapan(){
        $data = [
            'datasubkegiatanserapan' => $this->SubKegiatanSerapan->loadDataSerapanAkhir(),
            'datasubkegiatanserapanbybidang' => $this->SubKegiatanSerapan->loadDataSerapanAkhirbyBidang(Auth::user()->id_bidang),
            'datasubkegiatanserapanbyseksi' => $this->SubKegiatanSerapan->loadDataSerapanAkhirbySeksi(Auth::user()->id_seksi),
            'kegiatan' => $this->Kegiatan->loadData(),
            'subkegiatan' => $this->SubKegiatan->loadData(),
            'bidang' => $this->Bidang->loadData(),
            'subkegiatanindikator' => $this->SubKegiatanIndikator->loadData(),
            'kegiatanbybidang' => $this->Kegiatan->loadDatabyBidang(Auth::user()->id_bidang),
            'kegiatanbyseksi' => $this->Kegiatan->loadDatabySeksi(Auth::user()->id_seksi),
        ];
        return view('backend.datasubkegiatanserapan', $data);
    }

    public function datasubkegiatanserapanadd(){
        Request()->validate([
            'capaian_saat_ini' => 'required|max:150',
            'kegiatan' => 'required',
            'sub_kegiatan' => 'required',
        ],[
            'capaian_saat_ini.required' => 'Sasaran Sub Kegiatan harus diisi',
            'capaian_saat_ini.max' => 'Maksimal 150 karakter',
            'kegiatan.required' => 'Kegiatan harus dipilih',
            'sub_kegiatan.required' => 'Sub Kegiatan harus dipilih',
        ]);

        $data = [
            'id_sub_kegiatan' => Request()->sub_kegiatan,
            'capaian_lalu' => preg_replace(array('/\s+/','/R/','/p/','/\./'), array('','','',''), Request()->capaian_lalu),
            'capaian' => preg_replace(array('/\s+/','/R/','/p/','/\./'), array('','','',''), Request()->capaian_saat_ini),
            'created_at' => now(),
            'created_by' => Auth::user()->id,
        ];

        $this->SubKegiatanSerapan->addData($data);
        return redirect()->route('datasubkegiatanserapan')->with('pesan','data berhasil ditambahkan');
    }

//--- DATA SUB KEGIATAN FISIK ------------------------------------------------------------------------------------------------------------------------------------------

    public function datasubkegiatanfisik(){
        $data = [
            'datasubkegiatanfisik' => $this->SubKegiatanFisik->loadDataFisikAkhir(),
            'datasubkegiatanfisikbybidang' => $this->SubKegiatanFisik->loadDataFisikAkhirbyBidang(Auth::user()->id_bidang),
            'datasubkegiatanfisikbyseksi' => $this->SubKegiatanFisik->loadDataFisikAkhirbySeksi(Auth::user()->id_seksi),
            'kegiatan' => $this->Kegiatan->loadData(),
            'subkegiatan' => $this->SubKegiatan->loadData(),
            'bidang' => $this->Bidang->loadData(),
            'subkegiatanindikator' => $this->SubKegiatanIndikator->loadData(),
            'kegiatanbybidang' => $this->Kegiatan->loadDatabyBidang(Auth::user()->id_bidang),
            'kegiatanbyseksi' => $this->Kegiatan->loadDatabySeksi(Auth::user()->id_seksi),
        ];
        return view('backend.datasubkegiatanfisik', $data);
    }

    public function datasubkegiatanfisikadd(){
        Request()->validate([
            'capaian_saat_ini' => 'required|max:100',
            'kegiatan' => 'required',
            'sub_kegiatan' => 'required',
        ],[
            'capaian_saat_ini.required' => 'Sasaran Sub Kegiatan harus diisi',
            'capaian_saat_ini.max' => 'Maksimal 100 Persen',
            'kegiatan.required' => 'Kegiatan harus dipilih',
            'sub_kegiatan.required' => 'Sub Kegiatan harus dipilih',
        ]);

        $data = [
            'id_sub_kegiatan' => Request()->sub_kegiatan,
            'capaian_lalu' => Request()->capaian_lalu,
            'capaian' => Request()->capaian_saat_ini,
            'created_at' => now(),
            'created_by' => Auth::user()->id,
        ];

        $this->SubKegiatanFisik->addData($data);
        return redirect()->route('datasubkegiatanfisik')->with('pesan','data berhasil ditambahkan');
    }

//--- DATA LAYANAN ------------------------------------------------------------------------------------------------------------------------------------------

    public function datalayanan(){
        $data = [   
            'layanan' => $this->Layanan->loadData(),
            'bidang' => $this->Bidang->loadData()
        ];
        
        return view('backend.datalayanan', $data);
    }

    public function datalayananadd(){
        Request()->validate([
            'nama_layanan' => 'required|unique:bidang,nama|max:150',
            'nama_bidang' => 'required',
            'link' => 'required',
        ],[
            'nama_layanan.required' => 'Nama Layanan harus diisi',
            'nama_layanan.unique' => 'Nama Bidang sudah ada',
            'nama_layanan.max' => 'Maksimal 150 karakter',
            'nama_bidang.required' => 'Nama Bidang harus dipilih',
            'link.required' => 'Link harus diisi',
        ]);

        $data = [
            'nama' => Request()->nama_layanan,
            'link' => Request()->link,
            'created_at' => now(),
            'created_by' => Auth::user()->id,
            'updated_at' => now(),
            'updated_by' => Auth::user()->id,
            'id_bidang' => Request()->nama_bidang,
        ];

        $this->Layanan->addData($data);
        return redirect()->route('datalayanan')->with('pesan','data berhasil ditambahkan');
    }

    public function datalayanandelete($namabidang){
        $this->Layanan->deleteData($idlayanan);
        return view('backend.masterbidang', $data);
    }

//--- DATA BIDANG ------------------------------------------------------------------------------------------------------------------------------------------

    public function masterbidang(){
        if (Auth::user()->id_bidang>0){
            return redirect('/admin/mainmenu');
        }else{
            $data = ['bidang' => $this->Bidang->loadData()];
            return view('backend.masterbidang', $data);
        }
    }

    public function masterbidangadd(){
        Request()->validate([
            'nama_bidang' => 'required|unique:bidang,nama|max:150',
            'singkatan_bidang' => 'required|unique:bidang,nama|max:10',
            'ket_bidang' => 'required',
        ],[
            'nama_bidang.required' => 'Nama Bidang harus diisi',
            'nama_bidang.unique' => 'Nama Bidang sudah ada',
            'nama_bidang.max' => 'Maksimal 150 karakter',
            'singkatan_bidang.required' => 'Singaktan Nama Bidang harus diisi',
            'singkatan_bidang.unique' => 'Singaktan Nama Bidang sudah ada',
            'singkatan_bidang.max' => 'Maksimal 10 karakter',
            'ket_bidang.required' => 'Nama Bidang harus diisi',
        ]);

        $data = [
            'nama' => Request()->nama_bidang,
            'keterangan' => Request()->ket_bidang,
            'created_at' => now(),
            'created_by' => Auth::user()->id,
            'updated_at' => now(),
            'updated_by' => Auth::user()->id,
            'singkatan' => Request()->singkatan_bidang,
        ];

        $this->Bidang->addData($data);
        return redirect()->route('masterbidang')->with('pesan','data berhasil ditambahkan');
    }

    public function masterbidangdelete($idbidang){
        if (Auth::user()->id_bidang>0){
            return redirect('/admin/mainmenu');
        }else{
            $this->Bidang->deleteData($idbidang);
            return redirect()->route('masterbidang')->with('pesan','data berhasil dihapus');
        }
    }

//--- DATA SEKSI ------------------------------------------------------------------------------------------------------------------------------------------

    public function masterseksi(){
        if (Auth::user()->id_bidang>0){
            return redirect('/admin/mainmenu');
        }else{
            $data = ['seksi' => $this->Seksi->loadData(),'bidang' => $this->Bidang->loadData()];
            return view('backend.masterseksi', $data);
        }
    }
        
    public function masterseksiadd(){
        Request()->validate([
            'nama_seksi' => 'required|unique:seksi,nama|max:150',
            'nama_bidang' => 'required',
            'ket_seksi' => 'required',
        ],[
            'nama_seksi.required' => 'Nama Seksi harus diisi',
            'nama_seksi.unique' => 'Nama Seksi sudah ada',
            'nama_seksi.max' => 'Maksimal 150 karakter',
            'nama_bidang.required' => 'Nama Bidang harus dipilih',
            'ket_seksi.required' => 'Keterangan Seksi/Subbidang harus dipilih',
        ]);

        $data = [
            'nama' => Request()->nama_seksi,
            'id_bidang' => Request()->nama_bidang,
            'keterangan' => Request()->ket_seksi,
            'created_at' => now(),
            'created_by' => Auth::user()->id,
            'updated_at' => now(),
            'updated_by' => Auth::user()->id,
        ];

        $this->Seksi->addData($data);
        return redirect()->route('masterseksi')->with('pesan','data berhasil ditambahkan');
    }

    public function masterseksidelete($idseksi){
        if (Auth::user()->id_bidang>0){
            return redirect('/admin/mainmenu');
        }else{
            $this->Seksi->deleteData($idseksi);
            return redirect()->route('masterseksi')->with('pesan','data berhasil dihapus');
        }
    }

//--- DATA USER ------------------------------------------------------------------------------------------------------------------------------------------

    public function masteruser(){
        if (Auth::user()->id_bidang>0){
            return redirect('/admin/mainmenu');
        }else{
            $data = [
                        'pegawai' => $this->Users->loadData(),
                        'bidang' => $this->Bidang->loadData(),
                        'seksi' => $this->Seksi->loadData()
                    ];
            return view('backend.masteruser', $data);
        }
    }
        
    public function masteruseradd(){
        Request()->validate([
            'nama_pegawai' => 'required|max:150',
            'nip_pegawai' => 'required|unique:users,nip|max:150',
            'email_pegawai' => 'required|unique:users,email|max:150',
            'nama_bidang' => 'required',
        ],[
            'nama_pegawai.required' => 'Nama Pegawai harus diisi',
            'nama_pegawai.unique' => 'Nama Pegawai sudah ada',
            'nama_pegawai.max' => 'Maksimal 150 karakter',
            'nip_pegawai.required' => 'NIP Pegawai harus diisi',
            'nip_pegawai.unique' => 'NIP Pegawai sudah ada',
            'nip_pegawai.max' => 'Maksimal 150 karakter',
            'email_pegawai.required' => 'email Pegawai harus diisi',
            'email_pegawai.unique' => 'email Pegawai sudah ada',
            'email_pegawai.max' => 'Maksimal 150 karakter',
            'nama_bidang.required' => 'Bidang harus dipilih',
        ]);

        $data = [
            'name' => Request()->nama_pegawai,
            'email' => Request()->email_pegawai,
            'email_verified_at' => now(),
            'password' => Hash::make(Request()->nip_pegawai),
            'remember_token' => "",
            'created_at' => now(),
            'updated_at' => now(),
            'nip' => Request()->nip_pegawai,
            'id_role' => "",
            'id_bidang' => Request()->nama_bidang,
            'id_seksi' => Request()->nama_seksi,
        ];

        $this->User->addData($data);
        return redirect()->route('masteruser')->with('pesan','data berhasil ditambahkan');
    }

    public function masteruserdelete($idpegawai){
        if (Auth::user()->id_bidang>0){
            return redirect('/admin/mainmenu');
        }else{
            $this->User->deleteData($idpegawai);
            return redirect()->route('masteruser')->with('pesan','data berhasil dihapus');
        }
    }

//--- DATA SATUAN ------------------------------------------------------------------------------------------------------------------------------------------

    public function mastersatuan(){
        if (Auth::user()->id_bidang>0){
            return redirect('/admin/mainmenu');
        }else{
            $data = [
                        'satuan' => $this->Satuan->loadData()
                    ];
            return view('backend.mastersatuan', $data);
        }
    }

    public function mastersatuanadd(){
        Request()->validate([
            'nama_satuan' => 'required|unique:satuan,nama|max:150',
        ],[
            'nama_satuan.required' => 'Nama Satuan harus diisi',
            'nama_satuan.unique' => 'Nama Satuan sudah ada',
            'nama_satuan.max' => 'Maksimal 150 karakter',
        ]);

        $data = [
            'nama' => Request()->nama_satuan,
        ];

        $this->Satuan->addData($data);
        return redirect()->route('mastersatuan')->with('pesan','data berhasil ditambahkan');
    }

    public function mastersatuandelete($idsatuan){
        if (Auth::user()->id_bidang>0){
            return redirect('/admin/mainmenu');
        }else{
            $this->Satuan->deleteData($idsatuan);
            return redirect()->route('mastersatuan')->with('pesan','data berhasil dihapus');
        }
    }

//--- DROP DOWN ------------------------------------------------------------------------------------------------------------------------------------------

    public function loadseksi(Request $request){
        $seksi = Seksi::where("id_bidang",$request->id_bidang)->pluck('id','nama');
        return response()->json($seksi);
    }

    public function loadkegiatan(Request $request){
        $kegiatan = Kegiatan::where("id_bidang",$request->id_bidang)->pluck('id','nama');
        return response()->json($kegiatan);
    }

    public function loadsubkegiatan(Request $request){
        $subkegiatan = SubKegiatan::where("id_kegiatan",$request->id_kegiatan)->pluck('id','nama');
        return response()->json($subkegiatan);
    }

    public function loadkegiatanindikator(Request $request){
		$sasaran_kegiatan_indikator = KegiatanIndikator::where("id_kegiatan",$request->id_kegiatan)->pluck('id','sasaran');
		return response()->json($sasaran_kegiatan_indikator);
	}

    public function loadtargetkegiatanindikator(Request $request){
		$target_kegiatan_indikator = KegiatanIndikator::where("id",$request->id)->pluck('target','satuan');
		return response()->json($target_kegiatan_indikator);
	}

    public function loadanggarankegiatan(Request $request){
		$anggaran_kegiatan = Kegiatan::where("id",$request->id)->pluck('anggaran','nama');
		return response()->json($anggaran_kegiatan);
	}

    public function loadkegiatanindikatorlalu(Request $request){
		$kegiatan_indikator_lalu = KegiatanSasaran::where("id_indikator_kegiatan",$request->id_indikator_kegiatan)->orderBy('created_at','desc')->take(1)->pluck('capaian_lalu','capaian');
		return response()->json($kegiatan_indikator_lalu);
	}

    public function loadserapankegiatanlalu(Request $request){
		$serapan_kegiatan_lalu = KegiatanSerapan::where("id_kegiatan",$request->id_kegiatan)->orderBy('created_at','desc')->take(1)->pluck('capaian_lalu','capaian');
		return response()->json($serapan_kegiatan_lalu);
	}

    public function loadfisikkegiatanlalu(Request $request){
		$fisik_kegiatan_lalu = KegiatanFisik::where("id_kegiatan",$request->id_kegiatan)->orderBy('created_at','desc')->take(1)->pluck('capaian_lalu','capaian');
		return response()->json($fisik_kegiatan_lalu);
	}

    public function loadsubkegiatanindikator(Request $request){
		$sasaran_sub_kegiatan_indikator = SubKegiatanIndikator::where("id_sub_kegiatan",$request->id_sub_kegiatan)->pluck('id','sasaran');
		return response()->json($sasaran_sub_kegiatan_indikator);
	}
    
    public function loadtargetsubkegiatanindikator(Request $request){
		$target_sub_kegiatan_indikator = SubKegiatanIndikator::where("id",$request->id)->pluck('target','satuan');
		return response()->json($target_sub_kegiatan_indikator);
	}

    public function loadanggaransubkegiatan(Request $request){
		$anggaran_sub_kegiatan = SubKegiatan::where("id",$request->id)->pluck('anggaran','nama');
		return response()->json($anggaran_sub_kegiatan);
	}

    public function loadsubkegiatanbyseksi(Request $request){
        $subkegiatanbyseksi = SubKegiatan::where("id_seksi",Auth::user()->id_seksi)->where("id_kegiatan",$request->id_kegiatan)->pluck('id','nama');
        return response()->json($subkegiatanbyseksi);
    }

    public function loadsubkegiatanindikatorlalu(Request $request){
		$sub_kegiatan_indikator_lalu = SubKegiatanSasaran::where("id_indikator_sub_kegiatan",$request->id_indikator_sub_kegiatan)->orderBy('created_at','desc')->take(1)->pluck('capaian_lalu','capaian');
		return response()->json($sub_kegiatan_indikator_lalu);
	}

    public function loadserapansubkegiatanlalu(Request $request){
		$serapan_sub_kegiatan_lalu = SubKegiatanSerapan::where("id_sub_kegiatan",$request->id_sub_kegiatan)->orderBy('created_at','desc')->take(1)->pluck('capaian_lalu','capaian');
		return response()->json($serapan_sub_kegiatan_lalu);
	}

    public function loadfisiksubkegiatanlalu(Request $request){
		$fisik_sub_kegiatan_lalu = SubKegiatanFisik::where("id_sub_kegiatan",$request->id_sub_kegiatan)->orderBy('created_at','desc')->take(1)->pluck('capaian_lalu','capaian');
		return response()->json($fisik_sub_kegiatan_lalu);
	}

}
