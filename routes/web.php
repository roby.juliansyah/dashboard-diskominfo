<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;

use App\Http\Controllers\BidangController;
use App\Http\Controllers\BackendController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);

Route::get('/home', [HomeController::class, 'index']);

Route::get('/bidang/{nama}', [BidangController::class, 'detailbidang']);

Route::get('/admin/mainmenu', [BackendController::class, 'mainmenu'])->name('mainmenu');

//Route::get('/admin/userman', [BackendController::class, 'userman']);

// DPA -----------------------------------------------------------------------------

Route::get('/admin/dpakegiatan', [BackendController::class, 'dpakegiatan'])->name('dpakegiatan');
Route::post('/admin/dpakegiatanadd', [BackendController::class, 'dpakegiatanadd']);
Route::get('/admin/dpakegiatandelete/{id}', [BackendController::class, 'dpakegiatandelete']);

Route::get('/admin/dpakegiatanindikator', [BackendController::class, 'dpakegiatanindikator'])->name('dpakegiatanindikator');
Route::post('/admin/dpakegiatanindikatoradd', [BackendController::class, 'dpakegiatanindikatoradd']);
Route::get('/admin/dpakegiatanindikatordelete/{id}', [BackendController::class, 'dpakegiatanindikatordelete']);

Route::get('/admin/dpasubkegiatan', [BackendController::class, 'dpasubkegiatan'])->name('dpasubkegiatan');
Route::post('/admin/dpasubkegiatanadd', [BackendController::class, 'dpasubkegiatanadd']);
Route::get('/admin/dpasubkegiatandelete/{id}', [BackendController::class, 'dpasubkegiatandelete']);

Route::get('/admin/dpasubkegiatanindikator', [BackendController::class, 'dpasubkegiatanindikator'])->name('dpasubkegiatanindikator');
Route::post('/admin/dpasubkegiatanindikatoradd', [BackendController::class, 'dpasubkegiatanindikatoradd']);
Route::get('/admin/dpasubkegiatanindikatordelete/{id}', [BackendController::class, 'dpasubkegiatanindikatordelete']);

// Data Periodik -----------------------------------------------------------------------------

Route::get('/admin/datakegiatansasaran', [BackendController::class, 'datakegiatansasaran'])->name('datakegiatansasaran');
Route::post('/admin/datakegiatansasaranadd', [BackendController::class, 'datakegiatansasaranadd']);

Route::get('/admin/datakegiatanserapan', [BackendController::class, 'datakegiatanserapan'])->name('datakegiatanserapan');
Route::post('/admin/datakegiatanserapanadd', [BackendController::class, 'datakegiatanserapanadd']);

Route::get('/admin/datakegiatanfisik', [BackendController::class, 'datakegiatanfisik'])->name('datakegiatanfisik');
Route::post('/admin/datakegiatanfisikadd', [BackendController::class, 'datakegiatanfisikadd']);

Route::get('/admin/datasubkegiatansasaran', [BackendController::class, 'datasubkegiatansasaran'])->name('datasubkegiatansasaran');
Route::post('/admin/datasubkegiatansasaranadd', [BackendController::class, 'datasubkegiatansasaranadd']);

Route::get('/admin/datasubkegiatanserapan', [BackendController::class, 'datasubkegiatanserapan'])->name('datasubkegiatanserapan');
Route::post('/admin/datasubkegiatanserapanadd', [BackendController::class, 'datasubkegiatanserapanadd']);

Route::get('/admin/datasubkegiatanfisik', [BackendController::class, 'datasubkegiatanfisik'])->name('datasubkegiatanfisik');
Route::post('/admin/datasubkegiatanfisikadd', [BackendController::class, 'datasubkegiatanfisikadd']);

// Data Layanan -----------------------------------------------------------------------------

Route::get('/admin/datalayanan', [BackendController::class, 'datalayanan'])->name('datalayanan');
Route::post('/admin/datalayananadd', [BackendController::class, 'datalayananadd']);
Route::get('/admin/datalayanandelete/{id}', [BackendController::class, 'datalayanandelete']);

// Data Master -----------------------------------------------------------------------------

Route::get('/admin/masterbidang', [BackendController::class, 'masterbidang'])->name('masterbidang');
Route::post('/admin/masterbidangadd', [BackendController::class, 'masterbidangadd']);
Route::get('/admin/masterbidangdelete/{id}', [BackendController::class, 'masterbidangdelete']);

Route::get('/admin/masterseksi', [BackendController::class, 'masterseksi'])->name('masterseksi');
Route::post('/admin/masterseksiadd', [BackendController::class, 'masterseksiadd']);
Route::get('/admin/masterseksidelete/{id}', [BackendController::class, 'masterseksidelete']);

Route::get('/admin/masteruser', [BackendController::class, 'masteruser'])->name('masteruser');
Route::post('/admin/masteruseradd', [BackendController::class, 'masteruseradd']);
Route::get('/admin/masteruserdelete/{id}', [BackendController::class, 'masteruserdelete']);

Route::get('/admin/masterpegawai', [BackendController::class, 'masterpegawai'])->name('masterpegawai');
Route::post('/admin/masterpegawaiadd', [BackendController::class, 'masterpegawaiadd']);
Route::get('/admin/masterpegawaidelete/{id}', [BackendController::class, 'masterpegawaidelete']);

Route::get('/admin/mastersatuan', [BackendController::class, 'mastersatuan'])->name('mastersatuan');
Route::post('/admin/mastersatuanadd', [BackendController::class, 'mastersatuanadd']);
Route::get('/admin/mastersatuandelete/{id}', [BackendController::class, 'mastersatuandelete']);

// Load Data JSON -----------------------------------------------------------------------------

Route::get('/loadseksi', [BackendController::class, 'loadseksi']);
Route::get('/loadkegiatan', [BackendController::class, 'loadkegiatan']);
Route::get('/loadsubkegiatan', [BackendController::class, 'loadsubkegiatan']);
Route::get('/loadkegiatanindikator', [BackendController::class, 'loadkegiatanindikator']);
Route::get('/loadtargetkegiatanindikator', [BackendController::class, 'loadtargetkegiatanindikator']);
Route::get('/loadanggarankegiatan', [BackendController::class, 'loadanggarankegiatan']);
Route::get('/loadkegiatanindikatorlalu', [BackendController::class, 'loadkegiatanindikatorlalu']);
Route::get('/loadserapankegiatanlalu', [BackendController::class, 'loadserapankegiatanlalu']);
Route::get('/loadfisikkegiatanlalu', [BackendController::class, 'loadfisikkegiatanlalu']);
Route::get('/loadsubkegiatanindikator', [BackendController::class, 'loadsubkegiatanindikator']);
Route::get('/loadtargetsubkegiatanindikator', [BackendController::class, 'loadtargetsubkegiatanindikator']);
Route::get('/loadanggaransubkegiatan', [BackendController::class, 'loadanggaransubkegiatan']);
Route::get('/loadsubkegiatanbyseksi', [BackendController::class, 'loadsubkegiatanbyseksi']);
Route::get('/loadsubkegiatanindikatorlalu', [BackendController::class, 'loadsubkegiatanindikatorlalu']);
Route::get('/loadserapansubkegiatanlalu', [BackendController::class, 'loadserapansubkegiatanlalu']);
Route::get('/loadfisiksubkegiatanlalu', [BackendController::class, 'loadfisiksubkegiatanlalu']);

Auth::routes();

