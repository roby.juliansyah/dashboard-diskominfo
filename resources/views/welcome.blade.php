@extends('template.template')
@section('judul','Home')
@section('posisi','Home')
@section('lokasi')
<li class="breadcrumb-item active"><a href="#">Home</a></li>
@endsection

@section('sidebar')
  @foreach ($bidang as $bidangitem)
    <?php
      $bidangnospace = str_replace(' ', '', $bidangitem->nama);
      $bidangnospace = strtolower(str_replace(',', '', $bidangnospace));
    ?>
    <li class="nav-item">
      <a href="/bidang/{{$bidangitem->id}}" class="nav-link @yield('sidebar'.$bidangnospace)">
        <table>
          <td><i class="nav-icon fas fa-folder"></i></td>
          <td><p>{{$bidangitem->nama}}</p></td>
        </table>
      </a>
    </li>
  @endforeach
@endsection

<?php
  $namabidang = [];
  $color = [];
  $datatemp = [];
  $datatempserapan = [];
  $datatempfisik = [];
  $fisik = [];
  $data = [];
  $minus = [];
  $index = 0;
  $sasaran = 0;
  $performasum = 0;
  $performa = 0;
  $realisasianggaran = 0;
  $realisasifisik = 0;
  $performacolor = "#1E88E5";
?>

@section('konten')
    <div class="content-wrapper"> <!-- Content Wrapper. Contains page content -->
      <div class="content-header"> <!-- Content Header (Page header) -->
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6"></div> <!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                @yield('lokasi')
              </ol>
            </div> <!-- /.col -->
          </div> <!-- /.row -->
        </div> <!-- /.container-fluid -->
      </div> <!-- /.content-header -->
      <section class="content"> <!-- Main content -->
        <div class="container-fluid"> <!-- Small boxes (Stat box) -->
          @foreach ($bidang as $bidangitem)
            <!--<br>{{$bidangitem->singkatan}}-->
            @foreach ($datasubkegiatanfisik as $fisikitem)
              @if($fisikitem->id_bidang === $bidangitem->id)
                <?php
                  $datatempfisik[] = $fisikitem->capaian;
                ?>
                <!--<br> - {{$fisikitem->capaian}}-->
              @endif
            @endforeach
            @foreach ($datasubkegiatanserapan as $serapanitem)
              @if($serapanitem->id_bidang === $bidangitem->id)
                <?php
                  $datatempserapan[] = $serapanitem->capaian;
                ?>
                <!--<br> - {{$serapanitem->capaian}}-->
              @endif
            @endforeach
            @foreach ($datasubkegiatansasaran as $sasaranitem)
              @if($sasaranitem->id_bidang === $bidangitem->id)
                <?php
                  $namabidang[$index] = $bidangitem->singkatan;
                  $datatemp[] = number_format($sasaranitem->capaian / $sasaranitem->targetsasaran*100,0,"","");
                ?>
                <!--<br> - {{$sasaranitem->singkatan}} = {{$sasaranitem->capaian}} / {{$sasaranitem->targetsasaran}} -- {{number_format($sasaranitem->capaian / $sasaranitem->targetsasaran*100,0,"","")}}-->
              @endif
            @endforeach
            <?php
              $fisik[$index] = array_sum($datatempfisik)/count($datatempfisik);
              $datatempfisik = array();
              $data[$index] = number_format(array_sum($datatemp)/count($datatemp),0,"","");;
              $datatemp = array();
              $minus[$index] = 100 - $data[$index];
              if($data[$index] < 60){
                $color[$index] = "rgba(229,57,53, 1)";
              }elseif($data[$index] < 80){
                $color[$index] = "rgba(255,208,38, 1)";
              }else{
                $color[$index] = "rgba(22,167,92, 1)";
              }
              $index++;
              $realisasianggaran = array_sum($datatempserapan);
              $realisasifisik = number_format(array_sum($fisik)/$index,2,",",".");
              $performa = number_format(array_sum($data)/$index,2,",",".");
              if($performa < 60){
                $performacolor = "#E53835";
              }elseif($performa < 80){
                $performacolor = "#FFD026";
              }else{
                $performacolor = "#16A75C";
              }
            ?>            
          @endforeach
          
          @include('template.anggaranprimary')
          @include('template.charthome')
        </div>
      </section>
    </div>
@endsection
@section('script')
  <script>
    $(function () {
      /* ChartJS*/
      //- BAR CHART -
      var barChartCanvas = $('#barChart').get(0).getContext('2d')
      var barChartData = {
        labels  : {!! json_encode($namabidang) !!},
        datasets: [
          {
            label               : 'Pencapaian',
            backgroundColor     : {!! json_encode($color) !!},
            data                : {!! json_encode($data) !!}
          },
          {
            label               : 'Target',
            backgroundColor     : 'rgba(30,136,229, 0)',
            data                : {!! json_encode($minus) !!}
          },
        ]
      }

      var temp0 = barChartData.datasets[0]
      var temp1 = barChartData.datasets[1]
      barChartData.datasets[1] = temp1
      barChartData.datasets[0] = temp0

      var barChartOptions = {
        responsive              : true,
        datasetFill             : true,
        scales: {
          xAxes: [{ stacked: true }],
          yAxes: [{ stacked: true }]
        },
        legend: { display: false },
      }

      new Chart(barChartCanvas, {
        type: 'bar',
        data: barChartData,
        options: barChartOptions
      }) 
    })
  </script>
  <script>
    $(function () {
      /* ChartJS*/
      //- PIE CHART -
      var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
      var pieChartData = {
        labels  : {!! json_encode(["Positif","Negatif"]) !!},
        datasets: [
          {
            backgroundColor     : {!! json_encode(["#E53835","#16A75C"]) !!},
            data                : {!! json_encode([array_sum($data),array_sum($minus)]) !!}
          }
        ]
      }

      var pieChartOptions = {
        responsive              : true,
        datasetFill             : true,
        legend: { display: false },
      }

      new Chart(pieChartCanvas, {
        type: 'pie',
        data: pieChartData,
        options: pieChartOptions
      }) 
    })
  </script>

  <script>
    $(function () {
      function ini_events(ele) {
        ele.each(function () {
          var eventObject = {
            title: $.trim($(this).text())
          }
          $(this).data('eventObject', eventObject)
          $(this).draggable({
          zIndex        : 1070,
          revert        : true,
          revertDuration: 0
          })
        })
      }
      ini_events($('#external-events div.external-event'))
      var date = new Date()
      var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear()
      var Calendar = FullCalendar.Calendar;
      var Draggable = FullCalendar.Draggable;
      var containerEl = document.getElementById('external-events');
      var checkbox = document.getElementById('drop-remove');
      var calendarEl = document.getElementById('calendar');
      new Draggable(containerEl, {
        itemSelector: '.external-event',
        eventData: function(eventEl) {
          return {
          title: eventEl.innerText,
          backgroundColor: window.getComputedStyle( eventEl ,null).getPropertyValue('background-color'),
          borderColor: window.getComputedStyle( eventEl ,null).getPropertyValue('background-color'),
          textColor: window.getComputedStyle( eventEl ,null).getPropertyValue('color'),
          };
        }
      });
      var calendar = new Calendar(calendarEl, {
        headerToolbar: {
          left  : 'prev,next today',
          center: 'title',
          right : 'dayGridMonth,timeGridWeek,timeGridDay'
        },
        themeSystem: 'bootstrap',
        events: [
          {
            title          : 'All Day Event',
            start          : new Date(y, m, 1),
            backgroundColor: '#f56954', //red
            borderColor    : '#f56954', //red
            allDay         : true
          },
          {
            title          : 'Long Event',
            start          : new Date(y, m, d - 5),
            end            : new Date(y, m, d - 2),
            backgroundColor: '#f39c12', //yellow
            borderColor    : '#f39c12' //yellow
          },
          {
            title          : 'Meeting',
            start          : new Date(y, m, d, 10, 30),
            allDay         : false,
            backgroundColor: '#0073b7', //Blue
            borderColor    : '#0073b7' //Blue
          },
          {
            title          : 'Lunch',
            start          : new Date(y, m, d, 12, 0),
            end            : new Date(y, m, d, 14, 0),
            allDay         : false,
            backgroundColor: '#00c0ef', //Info (aqua)
            borderColor    : '#00c0ef' //Info (aqua)
          },
          {
            title          : 'Birthday Party',
            start          : new Date(y, m, d + 1, 19, 0),
            end            : new Date(y, m, d + 1, 22, 30),
            allDay         : false,
            backgroundColor: '#00a65a', //Success (green)
            borderColor    : '#00a65a' //Success (green)
          },
          {
            title          : 'Click for Google',
            start          : new Date(y, m, 28),
            end            : new Date(y, m, 29),
            url            : 'https://www.google.com/',
            backgroundColor: '#3c8dbc',
            borderColor    : '#3c8dbc'
          }
        ],
        editable  : true,
        droppable : true,
        drop      : function(info) {
          if (checkbox.checked) {
          info.draggedEl.parentNode.removeChild(info.draggedEl);
          }
        }
      });
      calendar.render();
      var currColor = '#3c8dbc' //Red by default
      $('#color-chooser > li > a').click(function (e) {
        e.preventDefault()
        currColor = $(this).css('color')
        $('#add-new-event').css({
          'background-color': currColor,
          'border-color'    : currColor
        })
      })
      $('#add-new-event').click(function (e) {
        e.preventDefault()
        var val = $('#new-event').val()
        if (val.length == 0) {
          return
        }
        var event = $('<div />')
        event.css({
          'background-color': currColor,
          'border-color'    : currColor,
          'color'           : '#fff'
        }).addClass('external-event')
        event.text(val)
        $('#external-events').prepend(event)
        ini_events(event)
        $('#new-event').val('')
      })
    })
  </script>
@endsection