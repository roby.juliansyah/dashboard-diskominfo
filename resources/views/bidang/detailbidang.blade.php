@foreach ($bidangbyid as $bidangbyiditem)
<?php
  $bidangbyidnospace = str_replace(' ', '', $bidangbyiditem->nama);
  $bidangbyidnospace = strtolower(str_replace(',', '', $bidangbyidnospace));
?>

@extends('template.template')
@section('judul',$bidangbyiditem->nama)
@section('posisi',$bidangbyiditem->nama)
@section('sidebar'.$bidangbyidnospace,'active')
@section('icon'.$bidangbyidnospace,'fa-folder-open')

@section('lokasi')
<li class="breadcrumb-item"><a href="/">Home</a></li>
<li class="breadcrumb-item active">{{ $bidangbyiditem->nama }}</li>
@endsection

@section('sidebar')
  @foreach ($bidang as $bidangitem)
    <?php
      $bidangnospace = str_replace(' ', '', $bidangitem->nama);
      $bidangnospace = strtolower(str_replace(',', '', $bidangnospace));
    ?>
    @if(auth()->user()->id_bidang === null)
      <li class="nav-item">
        <a href="/bidang/{{$bidangitem->id}}" class="nav-link @yield('sidebar'.$bidangnospace)">
          <table>
            <td><i class="nav-icon fas fa-folder @yield('icon'.$bidangnospace)"></i></td>
            <td><p>{{$bidangitem->nama}}</p></td>
          </table>
        </a>       
      </li>
    @endif
  @endforeach
@endsection

<?php
  $namaseksi = [];
  $datatempserapan = [];
  $datatempfisik = [];
  $datatemp = [];
  $fisik = [];
  $index = 0;
  $realisasianggaran = 0;
  $realisasifisik = 0;
?>

@section('konten')
  <div class="content-wrapper">
    
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6"></div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              @yield('lokasi')
            </ol>
          </div> 
        </div> 
      </div> 
    </div>

    <section class="content"> 
      <div class="container-fluid"> 
        @foreach ($datasubkegiatanfisik as $fisikitem)
          <?php
            $datatempfisik[$index] = $fisikitem->capaian;
            $index++;
          ?>
          <!-- <br> - {{$fisikitem->capaian}}   - Index = {{$index}}  -->
        @endforeach
        <?php
          if(count($datatempfisik)==0){
            $index = 0;
            $realisasifisik = number_format(100,2,",",".");
          }else{
            $realisasifisik = number_format(array_sum($datatempfisik)/$index,2,",",".");
            $index = 0;
          }
        ?>

        @foreach ($datasubkegiatanserapan as $serapanitem)
          <?php
            $datatempserapan[$index] = $serapanitem->capaian;
            $index++;
          ?>
          <!-- <br> - {{$serapanitem->capaian}} -->
        @endforeach
        <?php
          $realisasianggaran = array_sum($datatempserapan);
          $index = 0;
        ?>

        @foreach ($datasubkegiatansasaran as $sasaranitem)
          <?php
            $datatemp[$index] = number_format($sasaranitem->capaian / $sasaranitem->targetsasaran*100,0,"","");
            $index++;
          ?>
          <!-- <br> - {{$sasaranitem->singkatan}} = {{$sasaranitem->capaian}} / {{$sasaranitem->targetsasaran}} -- {{number_format($sasaranitem->capaian / $sasaranitem->targetsasaran*100,0,"","")}} -->
        @endforeach 
        <?php
          $performa = number_format(array_sum($datatemp)/$index,0,",",".");
          if($performa < 60){
            $performacolor = "danger";
          }elseif($performa < 80){
            $performacolor = "warning";
          }else{
            $performacolor = "success";
          }
        ?>

        @include('template.anggaranbidang')

        <h5>Performa Bidang</h5>
        <div class="progress">
          <div class="progress-bar bg-{{$performacolor}} progress-bar-striped" role="progressbar" aria-valuenow="{{$performa}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$performa}}%">
            {{$performa}}%
          </div>
        </div>
        <br>
      </div>
    </section>
  </div>
@endsection
@endforeach

@section('script')
@endsection