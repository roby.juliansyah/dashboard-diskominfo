@extends('template.templateadmin')
@section('judul','Kegiatan')
@section('posisi','Kegiatan')
@section('sidebardpakegiatan','active')
@section('sidebardpa','active')
@section('konten')
    @if (session('pesan'))
    <div class="alert alert-success alert-dismissible text-white">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h4><i class="icon fa fa-check text-white"></i>Sukses</h4>
      {{session('pesan')}}
    </div>
    @endif
    <div class="content-wrapper"> <!-- Content Wrapper. Contains page content -->
      <div class="content-header"> <!-- Content Header (Page header) -->
      </div> <!-- /.content-header -->
      <section class="content"> <!-- Main content -->
        <div class="container-fluid"> <!-- Small boxes (Stat box) -->
          <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Tambah Kegiatan Baru</h3>
            </div>
              <!-- /.card-header -->
              <!-- form start -->
            <form action="dpakegiatanadd" method="POST" enctype="multipart/form-data">
              @csrf
              <div class="card-body">
                <div class="form-group col-sm-6">
                  <label>Nama Kegiatan</label>
                  <input name="nama_kegiatan" class="form-control" placeholder="Masukkan Nama Kegiatan" value="{{ old('nama_kegiatan') }}">
                  <div class="text-danger">
                    @error('nama_kegiatan'){{$message}}
                    @enderror
                  </div>
                </div>
                @if(auth()->user()->id_bidang===null)
                  <div class="form-group col-sm-6">
                    <label>Pilih Bidang/UPTD</label>
                    <select name="nama_bidang" class="form-control" value="{{ old('nama_bidang') }}">
                      <option value=""> - Pilih - </option>
                      @foreach ($bidang as $bidangitem)
                      <option value="{{$bidangitem->id}}">{{$bidangitem->nama}} </option>
                      @endforeach
                    </select>
                    <div class="text-danger">
                      @error('nama_bidang'){{$message}}
                      @enderror
                    </div>
                  </div>
                @else
                  <div class="form-group col-sm-6">
                    <label>Pilih Bidang/UPTD</label>
                    @foreach ($bidang as $bidangitem)
                      @if(auth()->user()->id_bidang===$bidangitem->id)
                        <input name="nama_bidang" class="form-control" value="{{$bidangitem->id}}" type=hidden>
                        <input name="nama_bidang_show" class="form-control" value="{{$bidangitem->nama}}" readonly>
                      @endif
                    @endforeach
                  </div>
                @endif
                <div class="form-group col-sm-6">
                  <label>Kode Rekening</label>
                  <input name="kodering_kegiatan" class="form-control" placeholder="Masukkan Kode Rekening" value="{{ old('kodering_kegiatan') }}">
                  <div class="text-danger">
                    @error('kodering_kegiatan'){{$message}}
                    @enderror
                  </div>
                </div>
                <div class="form-group col-sm-6">
                  <label>Anggaran</label>
                  <input name="anggaran_kegiatan" id="rupiah" type="text" class="form-control" placeholder="Masukkan Nominal Anggaran" value="{{ old('anggaran_kegiatan') }}">
                  <div class="text-danger">
                    @error('anggaran_kegiatan'){{$message}}
                    @enderror
                  </div>
                </div>
                <div class="form-group col-sm-6">
                  <label>Tahun</label>
                  <input name="tahun_kegiatan" class="form-control" placeholder="Masukkan Tahun Kegiatan" value="{{$tahun}}" readonly>
                </div>
                
              </div> <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-info">Tambah</button>
              </div>
            </form>
          </div>

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Daftar Kegiatan</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th style="width: 10px">No</th>
                    <th>Nama Kegiatan</th>
                    <th>Nama Bidang/UPTD</th>
                    <th style="width: 150px">Anggaran</th>
                    <th style="width: 150px">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $nomor = 1 ?>
                  @if(auth()->user()->id_bidang===null)
                    <?php $datatable = $kegiatan; ?>
                  @else
                    <?php $datatable = $kegiatanbybidang; ?>
                  @endif
                  @foreach ($datatable as $item)
                  <tr>
                    <td>{{$nomor++}}</td>
                    <td>{{$item->nama}}</td>
                    <td>{{$item->bidang}}</td>
                    <td class="text-right">@currency($item->anggaran)</td>
                    <td>
                      <a href="#" class="btn btn-sm btn-warning">Update</a>
                      <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#delete{{$item->id}}">
                        Delete
                      </button>
                    </td>
                  </tr>
                  
                  @endforeach
                </tbody>
              </table>
              <br><br><br>
            </div>
            <!-- /.card-body -->
          </div>
        </div><!-- /.container-fluid -->
      </section>
    </div>

    @foreach ($datatable as $item)
      <div class="modal fade" id="delete{{$item->id}}">
        <div class="modal-dialog">
          <div class="modal-content bg-danger">
            <div class="modal-header">
              <h5 class="modal-title">Delete Data</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Anda yakin akan menghapus "{{$item->nama}}" dari daftar?</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-outline pull-left text-white" data-dismiss="modal">Cancel</button>
              <a href="/admin/dpakegiatandelete/{{ $item->id }}" class="btn btn-outline">
              <button type="button" class="btn btn-outline text-white">Delete</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
    @endforeach

@endsection

