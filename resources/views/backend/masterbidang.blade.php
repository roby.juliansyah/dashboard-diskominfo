@extends('template.templateadmin')
@section('judul','Master Bidang/UPTD')
@section('posisi','Master Bidang/UPTD')
@section('sidebarmasterbidang','active')
@section('sidebarmaster','active')
@section('konten')
    <div class="content-wrapper"> <!-- Content Wrapper. Contains page content -->
      <div class="content-header"> <!-- Content Header (Page header) -->
      </div> <!-- /.content-header -->
      <section class="content"> <!-- Main content -->
        <div id="toastsContainerTopRight" class="toasts-top-right fixed">
          @if (session('pesan'))
            <div class="alert alert-success alert-dismissible text-white">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h6><i class="icon fa fa-check text-white"></i>Sukses</h6>
              {{session('pesan')}}
            </div>
          @endif    
        </div>
        <div class="container-fluid"> <!-- Small boxes (Stat box) -->
          <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Tambah Bidang/UPTD Baru</h3>
            </div>
              <!-- /.card-header -->
              <!-- form start -->
            <form action="masterbidangadd" method="POST" enctype="multipart/form-data">
              @csrf
              <div class="card-body">
              <div class="form-group col-sm-6">
                  <label>Nama Bidang/UPTD</label>
                  <input name="nama_bidang" class="form-control" placeholder="Masukkan Nama Bidang" value="{{ old('nama_bidang') }}">
                  <div class="text-danger">
                    @error('nama_bidang'){{$message}}
                    @enderror
                  </div>
                </div>
                <div class="form-group col-sm-6">
                  <label>Singkatan Nama Bidang/UPTD</label>
                  <input name="singkatan_bidang" class="form-control" placeholder="Masukkan Singkatan Nama Bidang" value="{{ old('singkatan_bidang') }}">
                  <div class="text-danger">
                    @error('singkatan_bidang'){{$message}}
                    @enderror
                  </div>
                </div>
                <div class="form-group col-sm-6">
                  <label>Keterangan</label>
                  <select name="ket_bidang" class="form-control" value="{{ old('ket_bidang') }}">
                    <option value=""> - Pilih - </option>
                    <option value="Bidang"> Bidang </option>
                    <option value="UPTD"> UPTD </option>
                  </select>
                  <div class="text-danger">
                    @error('ket_bidang'){{$message}}
                    @enderror
                  </div>
                </div>
              </div> <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-info">Tambah</button>
              </div>
            </form>
          </div>

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Daftar Bidang</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th style="width: 10px">No</th>
                    <th>Nama Bidang</th>
                    <th>Keterangan</th>
                    <th style="width: 150px">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $nomor = 1 ?>
                  @foreach ($bidang as $item)
                  <tr>
                    <td>{{$nomor++}}</td>
                    <td>{{$item->nama}}</td>
                    <td>{{$item->keterangan}}</td>
                    <td>
                      <a href="#" class="btn btn-sm btn-warning">Update</a>
                      <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#delete{{$item->id}}">
                        Delete
                      </button>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              <br><br><br>
            </div>
            <!-- /.card-body -->
          </div>
        </div><!-- /.container-fluid -->
      </section>
    </div>

    @foreach ($bidang as $item)
      <div class="modal fade" id="delete{{$item->id}}">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Delete Data</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Anda yakin akan menghapus "{{$item->nama}}" dari daftar?</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <a href="/admin/masterbidangdelete/{{ $item->id }}" class="btn btn-outline">
              <button type="button" class="btn btn-danger text-white">Delete</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
    @endforeach

@endsection

