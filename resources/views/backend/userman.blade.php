@extends('template.templateadmin')
@section('judul','User Management')
@section('posisi','User Management')
@section('userman','active')
@section('konten')
    <div class="content-wrapper"> <!-- Content Wrapper. Contains page content -->
      <div class="content-header"> <!-- Content Header (Page header) -->
      </div> <!-- /.content-header -->
      <section class="content"> <!-- Main content -->
        <div class="container-fluid"> <!-- Small boxes (Stat box) -->
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Add New User</h3>
            </div>
              <!-- /.card-header -->
              <!-- form start -->
            <form>
            <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile">File input</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="exampleInputFile">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text">Upload</span>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">User List</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th style="width: 10px">No</th>
                      <th>Email</th>
                      <th>Level User</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1.</td>
                      <td>superadmin@diskominfo.jabarprov.go.id</td>
                      <td>
                        Super Admin
                      </td>
                      <td>
                        <span class="badge bg-warning">Update</span>
                        <span class="badge bg-danger">Delete</span>
                      </td>
                    </tr>
                    <tr>
                      <td>2.</td>
                      <td>setiaji@jabarprov.go.id</td>
                      <td>
                        Kepala Dinas
                      </td>
                      <td>
                        <span class="badge bg-warning">Update</span>
                        <span class="badge bg-danger">Delete</span>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <br><br><br>
              </div>
              <!-- /.card-body -->
            </div>
        </div><!-- /.container-fluid -->
      </section>
    </div>
          
@endsection
