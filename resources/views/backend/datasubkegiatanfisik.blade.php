@extends('template.templateadmin')
@section('judul','Realisasi Fisik Sub Kegiatan')
@section('posisi','Realisasi Fisik Sub Kegiatan')
@section('sidebardatasubkegiatanfisik','active')
@section('sidebardatasubkegiatan','active')
@section('sidebardata','active')
@section('konten')
    @if (session('pesan'))
    <div class="alert alert-success alert-dismissible text-white">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h4><i class="icon fa fa-check text-white"></i>Sukses</h4>
      {{session('pesan')}}
    </div>
    @endif
    <div class="content-wrapper"> <!-- Content Wrapper. Contains page content -->
      <div class="content-header"> <!-- Content Header (Page header) -->
      </div> <!-- /.content-header -->
      <section class="content"> <!-- Main content -->
        <div class="container-fluid"> <!-- Small boxes (Stat box) -->
          <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Tambah Realisasi Fisik Sub Kegiatan Baru</h3>
            </div>
              <!-- /.card-header -->
              <!-- form start -->
            <form action="datasubkegiatanfisikadd" method="POST" enctype="multipart/form-data">
              @csrf
              <div class="card-body">
                @if(auth()->user()->id_bidang===null)
                  <div class="form-group col-sm-6">
                    <label>Pilih Bidang/UPTD</label>
                    <select name="nama_bidang" class="form-control" id="nama_bidang">
                      <option value=""> - Pilih - </option>
                      @foreach ($bidang as $bidangitem)
                      <option value="{{$bidangitem->id}}">{{$bidangitem->nama}}</option>
                      @endforeach
                    </select>
                    <div class="text-danger">
                      @error('nama_bidang'){{$message}}
                      @enderror
                    </div>
                  </div>
                  <div class="form-group col-sm-6">
                    <label>Pilih Kegiatan</label>
                    <select class="form-control" name="kegiatan" id="nama_kegiatan" disabled>
                      <option value="" selected> - Pilih - </option>
                    </select>
                    <div class="text-danger">
                      @error('kegiatan'){{$message}}
                      @enderror
                    </div>
                  </div>
                @else
                  <div class="form-group col-sm-6">
                    <label>Pilih Bidang/UPTD</label>
                    @foreach ($bidang as $bidangitem)
                      @if(auth()->user()->id_bidang===$bidangitem->id)
                        <input name="nama_bidang" class="form-control" value="{{$bidangitem->id}}" type=hidden>
                        <input name="nama_bidang_show" class="form-control" value="{{$bidangitem->nama}}" readonly>
                      @endif
                    @endforeach
                  </div>
                  <div class="form-group col-sm-6">
                    <label>Pilih Kegiatan</label>
                    @if(auth()->user()->id_seksi===null)
                      <select name="kegiatan" class="form-control" id="nama_kegiatan">
                        <option value=""> - Pilih - </option>
                        @foreach ($kegiatanbybidang as $kegiatanitem)
                          <option value="{{$kegiatanitem->id}}">{{$kegiatanitem->nama}}</option>
                        @endforeach
                      </select>  
                    @else
                      <select name="kegiatan" class="form-control" id="nama_kegiatan_by_seksi">
                        <option value=""> - Pilih - </option>
                        @foreach ($kegiatanbyseksi as $kegiatanitem)
                          <option value="{{$kegiatanitem->id}}">{{$kegiatanitem->nama}}</option>
                        @endforeach
                      </select>
                    @endif
                    <div class="text-danger">
                      @error('kegiatan'){{$message}}
                      @enderror
                    </div>
                  </div>
                @endif
                <div class="form-group col-sm-6">
                  <label>Pilih Sub Kegiatan</label>
                  @if(auth()->user()->id_seksi===null)
                    <select class="form-control" name="sub_kegiatan" id="nama_sub_kegiatan" disabled>
                      <option value="" selected> - Pilih - </option>
                    </select>
                  @else
                    <select class="form-control" name="sub_kegiatan" id="nama_sub_kegiatan" disabled>
                      <option value="" selected> - Pilih - </option>
                    </select>
                  @endif
                  <div class="text-danger">
                    @error('sub_kegiatan'){{$message}}
                    @enderror
                  </div>
                </div>
                <div class="form-group col-sm-6">
                  <label>Realisasi Fisik Sebelumnya (%)</label>
                  <div id="fisik_sub_kegiatan_lalu">
                    <input name="capaian_lalu" class="form-control" value="0" readonly>
                  </div>
                </div>
                <div class="form-group col-sm-6">
                  <label>Realisasi Fisik Saat Ini (%)</label>
                  <input name="capaian_saat_ini" type="number" class="form-control" placeholder="Masukkan Realisasi Saat Ini" value="{{ old('capaian_saat_ini') }}">
                  <div class="text-danger">
                    @error('capaian_saat_ini'){{$message}}
                    @enderror
                  </div>
                </div>
              </div> <!-- /.card-body -->
              <div class="card-footer">
                <button type="submit" class="btn btn-info">Tambah</button>
              </div>
            </form>
          </div>

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Daftar Realisasi Fisik Sub Kegiatan</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th style="width: 10px">No</th>
                    <th style="width: 250px">Nama Seksi/subbidang</th>
                    <th>Nama Sub Kegiatan</th>
                    <th style="width: 250px">Tanggal Input</th>
                    <th>Capaian Terakhir</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $nomor = 1 ?>
                  @if(auth()->user()->id_bidang===null)
                    <?php $datatable = $datasubkegiatanfisik; ?>
                  @elseif(auth()->user()->id_seksi===null)
                    <?php $datatable = $datasubkegiatanfisikbybidang; ?>
                  @else
                    <?php $datatable = $datasubkegiatanfisikbyseksi; ?>
                  @endif
                  @foreach ($datatable as $item)
                  <tr>
                    <td>{{$nomor++}}</td>
                    <td>{{$item->seksi}}</td>
                    <td>{{$item->sub_kegiatan}}</td>
                    <?php $tanggal = date('l, d F Y', strtotime($item->created_at)); ?>
                    <td>{{$tanggal}}</td>
                    <td class="text-right">{{$item->capaian}} %</td>
                  </tr>
                  
                  @endforeach
                </tbody>
              </table>
              <br><br><br>
            </div>
            <!-- /.card-body -->
          </div>
        </div><!-- /.container-fluid -->
      </section>
    </div>

@endsection

