@extends('template.templateadmin')
@section('judul','Master Satuan')
@section('posisi','Master Satuan')
@section('sidebarmastersatuan','active')
@section('sidebarmaster','active')
@section('konten')
    <div class="content-wrapper"> <!-- Content Wrapper. Contains page content -->
      <div class="content-header"> <!-- Content Header (Page header) -->
      </div> <!-- /.content-header -->
      <section class="content"> <!-- Main content -->
        <div id="toastsContainerTopRight" class="toasts-top-right fixed">
          @if (session('pesan'))
            <div class="alert alert-success alert-dismissible text-white">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h6><i class="icon fa fa-check text-white"></i>Sukses</h6>
              {{session('pesan')}}
            </div>
          @endif    
        </div>
        <div class="container-fluid"> <!-- Small boxes (Stat box) -->
          <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Tambah Satuan Baru</h3>
            </div>
              <!-- /.card-header -->
              <!-- form start -->
            <form action="mastersatuanadd" method="POST" enctype="multipart/form-data">
              @csrf
              <div class="card-body">
                <div class="form-group col-sm-6">
                  <label>Nama Satuan</label>
                  <input name="nama_satuan" class="form-control" placeholder="Masukkan Nama Satuan" value="{{ old('nama_satuan') }}">
                  <div class="text-danger">
                    @error('nama_satuan'){{$message}}
                    @enderror
                  </div>
                </div>
              </div> <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-info">Tambah</button>
              </div>
            </form>
          </div>

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Daftar Satuan</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th style="width: 10px">No</th>
                    <th>Nama Satuan</th>
                    <th style="width: 150px">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $nomor = 1 ?>
                  @foreach ($satuan as $item)
                  <tr>
                    <td>{{$nomor++}}</td>
                    <td>{{$item->nama}}</td>
                    <td>
                      <a href="#" class="btn btn-sm btn-warning">Update</a>
                      <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#delete{{$item->id}}">
                        Delete
                      </button>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              <br><br><br>
            </div>
            <!-- /.card-body -->
          </div>
        </div><!-- /.container-fluid -->
      </section>
    </div>

    @foreach ($satuan as $item)
      <div class="modal fade" id="delete{{$item->id}}">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Delete Data</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Anda yakin akan menghapus "{{$item->nama}}" dari daftar?</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <a href="/admin/mastersatuandelete/{{ $item->id }}" class="btn btn-outline">
              <button type="button" class="btn btn-danger text-white">Delete</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
    @endforeach

@endsection

