@extends('template.templateadmin')
@section('judul','Master User')
@section('posisi','Master User')
@section('sidebarmasteruser','active')
@section('sidebarmaster','active')
@section('konten')
    <div class="content-wrapper"> <!-- Content Wrapper. Contains page content -->
      <div class="content-header"> <!-- Content Header (Page header) -->
      </div> <!-- /.content-header -->
      <section class="content"> <!-- Main content -->
        <div id="toastsContainerTopRight" class="toasts-top-right fixed">
          @if (session('pesan'))
            <div class="alert alert-success alert-dismissible text-white">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h6><i class="icon fa fa-check text-white"></i>Sukses</h6>
              {{session('pesan')}}
            </div>
          @endif    
        </div>
        <div class="container-fluid"> <!-- Small boxes (Stat box) -->
          <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Tambah User Baru</h3>
            </div>
              <!-- /.card-header -->
              <!-- form start -->
            <form action="masterpegawaiadd" method="POST" enctype="multipart/form-data">
              @csrf
              <div class="card-body">
                <div class="form-group col-sm-6">
                  <label>Nama User</label>
                  <input name="nama_pegawai" class="form-control" placeholder="Masukkan Nama Pegawai" value="{{ old('nama_pegawai') }}">
                  <div class="text-danger">
                    @error('nama_pegawai'){{$message}}
                    @enderror
                  </div>
                </div>
                <div class="form-group col-sm-6">
                  <label>NIP User</label>
                  <input name="nip_pegawai" class="form-control" placeholder="Masukkan NIP Pegawai" value="{{ old('nip_pegawai') }}">
                  <div class="text-danger">
                    @error('nip_pegawai'){{$message}}
                    @enderror
                  </div>
                </div>
                <div class="form-group col-sm-6">
                  <label>email User</label>
                  <input name="email_pegawai" class="form-control" placeholder="Masukkan email Pegawai" value="{{ old('email_pegawai') }}">
                  <div class="text-danger">
                    @error('email_pegawai'){{$message}}
                    @enderror
                  </div>
                </div>
                <div class="form-group col-sm-6">
                  <label>Pilih Bidang/UPTD</label>
                  <select name="nama_bidang" class="form-control" id="nama_bidang">
                    <option value=""> - Pilih - </option>
                    @foreach ($bidang as $bidangitem)
                    <option value="{{$bidangitem->id}}">{{$bidangitem->nama}} </option>
                    @endforeach
                  </select>
                  <div class="text-danger">
                    @error('nama_bidang'){{$message}}
                    @enderror
                  </div>
                </div>

                <div class="form-group col-sm-6">
                  <label>Pilih Seksi/Subbagian</label>
                  <select class="form-control" name="nama_seksi" id="nama_seksi" disabled>
                    <option value="" selected> - Pilih - </option>
                  </select>
                  <div class="text-danger">
                    @error('nama_seksi'){{$message}}
                    @enderror
                  </div>
                </div>

              <div class="card-footer">
                <button type="submit" class="btn btn-info">Tambah</button>
              </div>
            </form>
          </div>

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Daftar User</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th style="width: 10px">No</th>
                    <th>Nama User</th>
                    <th>email</th>
                    <th>Jabatan</th>
                    <th style="width: 150px">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $nomor = 1 ?>
                  @foreach ($pegawai as $item)
                  <tr>
                    <td>{{$nomor++}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->email}}</td>
                    @if($item->id_bidang === null)
                      <td></td>
                    @elseif($item->id_seksi === null)
                      @if($item->bidang === "Sekretariat")
                        <td>Sekretaris Dinas/Badan</td>
                      @else
                        <td>Kepala {{$item->bidangket}} {{$item->bidang}}</td>
                      @endif
                    @else
                      <td>Kepala {{$item->seksiket}} {{$item->seksi}}</td>
                    @endif
                    <td>
                      <a href="#" class="btn btn-sm btn-warning">Update</a>
                      <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#delete{{$item->id}}">
                        Delete
                      </button>
                    </td>
                  </tr>
                  
                  @endforeach
                </tbody>
              </table>
              <br><br><br>
            </div>
            <!-- /.card-body -->
          </div>
        </div><!-- /.container-fluid -->
      </section>
    </div>

    @foreach ($pegawai as $item)
      <div class="modal fade" id="delete{{$item->id}}">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Delete Data</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Anda yakin akan menghapus "{{$item->name}}" dari daftar?</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <a href="/admin/masterpegawaidelete/{{ $item->id }}" class="btn btn-outline">
              <button type="button" class="btn btn-danger text-white">Delete</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
    @endforeach

@endsection

