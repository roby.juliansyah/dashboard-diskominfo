@extends('template.templateadmin')
@section('judul','Main Menu')
@section('posisi','Main Menu')
@section('konten')
    <div class="content-wrapper"> <!-- Content Wrapper. Contains page content -->
      <div class="content-header"> <!-- Content Header (Page header) -->
      </div> <!-- /.content-header -->
      <section class="content"> <!-- Main content -->
        <div class="container-fluid"> <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-2 col-6"> <!-- small box -->
              <div class="small-box bg-success">
                <div class="inner">
                  <p>Total Bidang & UPTD</p>
                  <h3>{{ $bidang->count() }}</h3>
                </div>
                <div class="icon">
                  <i class="ion ion-person-stalker"></i>
                </div>
              </div>
            </div> <!-- ./col -->
            <div class="col-lg-2 col-6"> <!-- small box -->
              <div class="small-box bg-success">
                <div class="inner">
                  <p>Total Seksi & Subbagian</p>
                  <h3>{{ $seksi->count() }}</h3>
                </div>
                <div class="icon">
                  <i class="ion ion-person-stalker"></i>
                </div>
              </div>
            </div> <!-- ./col -->
            <div class="col-lg-2 col-6"> <!-- small box -->
              <div class="small-box bg-primary">
                <div class="inner">
                  <p>Total Pegawai</p>
                  <h3>{{ $pegawai->count() }}</h3>
                </div>
                <div class="icon">
                  <i class="ion ion-person"></i>
                </div> 
              </div>
            </div>
            <!--<div class="col-lg-2 col-6"> 
              <div class="small-box bg-info">
                <div class="inner">
                  <p>Total Pegawai Non-ASN</p>
                  <h3>250</h3>
                </div>
                <div class="icon">
                  <i class="ion ion-person"></i>
                </div>
              </div>
            </div> -->
            <div class="col-lg-2 col-6"> <!-- small box -->
              <div class="small-box bg-danger">
                <div class="inner">
                  <p>Total KPI Kegiatan</p>
                  <h3>{{ $kpikegiatan->count() }}</h3>
                </div>
                <div class="icon">
                  <i class="ion ion-key"></i>
                </div>
              </div>
            </div> <!-- ./col -->
            <div class="col-lg-2 col-6"> <!-- small box -->
              <div class="small-box bg-danger">
                <div class="inner">
                  <p>Total KPI Sub Kegiatan</p>
                  <h3>{{ $kpisubkegiatan->count() }}</h3>
                </div>
                <div class="icon">
                  <i class="ion ion-key"></i>
                </div>
              </div>
            </div> <!-- ./col -->
            <div class="col-lg-2 col-6"> <!-- small box -->
              <div class="small-box bg-secondary">
                <div class="inner">
                  <p>Total Layanan</p>
                  <h3>{{ $layanan->count() }}</h3>
                </div>
                <div class="icon">
                  <i class="ion ion-information"></i>
                </div>
              </div>
            </div> <!-- ./col -->
          </div> <!-- /.row -->
        </div><!-- /.container-fluid -->
      </section>
    </div>
          
@endsection
