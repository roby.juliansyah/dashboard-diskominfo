@extends('template.templateadmin')
@section('judul','Indikator Sub Kegiatan')
@section('posisi','Indikator Sub Kegiatan')
@section('sidebardpasubkegiatanindikator','active')
@section('sidebardpa','active')
@section('konten')
    @if (session('pesan'))
    <div class="alert alert-success alert-dismissible text-white">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h4><i class="icon fa fa-check text-white"></i>Sukses</h4>
      {{session('pesan')}}
    </div>
    @endif
    <div class="content-wrapper"> <!-- Content Wrapper. Contains page content -->
      <div class="content-header"> <!-- Content Header (Page header) -->
      </div> <!-- /.content-header -->
      <section class="content"> <!-- Main content -->
        <div class="container-fluid"> <!-- Small boxes (Stat box) -->
          <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Tambah Indikator Sub Kegiatan Baru</h3>
            </div>
              <!-- /.card-header -->
              <!-- form start -->
            <form action="dpasubkegiatanindikatoradd" method="POST" enctype="multipart/form-data">
              @csrf
              <div class="card-body">
                <div class="form-group col-sm-6">
                  <label>Sasaran Sub Kegiatan</label>
                  <input name="sasaran_sub_kegiatan" class="form-control" placeholder="Masukkan Sasaran Sub Kegiatan" value="{{ old('sasaran_sub_kegiatan') }}">
                  <div class="text-danger">
                    @error('sasaran_sub_kegiatan'){{$message}}
                    @enderror
                  </div>
                </div>
                @if(auth()->user()->id_bidang===null)
                  <div class="form-group col-sm-6">
                    <label>Pilih Bidang/UPTD</label>
                    <select name="nama_bidang" class="form-control" id="nama_bidang">
                      <option value=""> - Pilih - </option>
                      @foreach ($bidang as $bidangitem)
                      <option value="{{$bidangitem->id}}">{{$bidangitem->nama}}</option>
                      @endforeach
                    </select>
                    <div class="text-danger">
                      @error('nama_bidang'){{$message}}
                      @enderror
                    </div>
                  </div>
                  <div class="form-group col-sm-6">
                    <label>Pilih Kegiatan</label>
                    <select class="form-control" name="nama_kegiatan" id="nama_kegiatan" disabled>
                      <option value="" selected> - Pilih - </option>
                    </select>
                    <div class="text-danger">
                      @error('nama_kegiatan'){{$message}}
                      @enderror
                    </div>
                  </div>
                @else
                  <div class="form-group col-sm-6">
                    <label>Pilih Bidang/UPTD</label>
                    @foreach ($bidang as $bidangitem)
                      @if(auth()->user()->id_bidang===$bidangitem->id)
                        <input name="nama_bidang" class="form-control" value="{{$bidangitem->id}}" type=hidden>
                        <input name="nama_bidang_show" class="form-control" value="{{$bidangitem->nama}}" readonly>
                      @endif
                    @endforeach
                  </div>
                  <div class="form-group col-sm-6">
                    <label>Pilih Kegiatan</label>
                    @if(auth()->user()->id_seksi===null)
                      <select name="nama_kegiatan" class="form-control" id="nama_kegiatan">
                        <option value=""> - Pilih - </option>
                        @foreach ($kegiatanbybidang as $kegiatanitem)
                          <option value="{{$kegiatanitem->id}}">{{$kegiatanitem->nama}}</option>
                        @endforeach
                      </select>  
                    @else
                      <select name="nama_kegiatan" class="form-control" id="nama_kegiatan_by_seksi">
                        <option value=""> - Pilih - </option>
                        @foreach ($kegiatanbyseksi as $kegiatanitem)
                          <option value="{{$kegiatanitem->id}}">{{$kegiatanitem->nama}}</option>
                        @endforeach
                      </select>
                    @endif
                    <div class="text-danger">
                      @error('nama_kegiatan'){{$message}}
                      @enderror
                    </div>
                  </div>
                @endif
                <div class="form-group col-sm-6">
                  <label>Pilih Sub Kegiatan</label>
                  @if(auth()->user()->id_seksi===null)
                    <select class="form-control" name="nama_sub_kegiatan" id="nama_sub_kegiatan" disabled>
                      <option value="" selected> - Pilih - </option>
                    </select>
                  @else
                    <select class="form-control" name="nama_sub_kegiatan" id="nama_sub_kegiatan" disabled>
                      <option value="" selected> - Pilih - </option>
                    </select>
                  @endif
                  <div class="text-danger">
                    @error('nama_sub_kegiatan'){{$message}}
                    @enderror
                  </div>
                </div>
                <div class="form-group col-sm-6">
                  <label>Indikator</label>
                  <input name="indikator_sub_kegiatan" class="form-control" placeholder="Masukkan Indikator Sub Kegiatan" value="{{ old('indikator_sub_kegiatan') }}">
                  <div class="text-danger">
                    @error('indikator_sub_kegiatan'){{$message}}
                    @enderror
                  </div>
                </div>
                <div class="form-group col-sm-6">
                  <label>Target</label>
                  <input name="target_sub_kegiatan" class="form-control" placeholder="Masukkan Target Sub Kegiatan" value="{{ old('target_sub_kegiatan') }}">
                  <div class="text-danger">
                    @error('target_sub_kegiatan'){{$message}}
                    @enderror
                  </div>
                </div>
                <div class="form-group col-sm-6">
                  <label>Satuan</label>
                  <input name="satuan_target" class="form-control" placeholder="Masukkan Satuan Target" value="{{ old('satuan_target') }}">
                  <div class="text-danger">
                    @error('satuan_target'){{$message}}
                    @enderror
                  </div>
                </div>
                
              </div> <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-info">Tambah</button>
              </div>
            </form>
          </div>

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Daftar Indikator Sub Kegiatan</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th style="width: 10px">No</th>
                    <th style="width: 200px">Nama Seksi/subbidang</th>
                    <th>Nama Sub Kegiatan</th>
                    <th>Sasaran Sub Kegiatan</th>
                    <th>Target</th>
                    <th>Satuan</th>
                    <th style="width: 150px">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $nomor = 1 ?>
                  @if(auth()->user()->id_bidang===null)
                    <?php $datatable = $subkegiatanindikator; ?>
                  @else
                    @if(auth()->user()->id_seksi===null)
                      <?php $datatable = $subkegiatanindikatorbybidang; ?>
                    @else
                      <?php $datatable = $subkegiatanindikatorbyseksi; ?>
                    @endif
                  @endif
                  @foreach ($datatable as $item)
                  <tr>
                    <td>{{$nomor++}}</td>
                    <td>{{$item->seksi}}</td>
                    <td>{{$item->sub_kegiatan}}</td>
                    <td>{{$item->sasaran}}</td>
                    <td class="text-right">{{$item->target}}</td>
                    <td>{{$item->satuan}}</td>
                    <td>
                      <a href="#" class="btn btn-sm btn-warning">Update</a>
                      <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#delete{{$item->id}}">
                        Delete
                      </button>
                    </td>
                  </tr>
                  
                  @endforeach
                </tbody>
              </table>
              <br><br><br>
            </div>
            <!-- /.card-body -->
          </div>
        </div><!-- /.container-fluid -->
      </section>
    </div>

    @foreach ($datatable as $item)
      <div class="modal fade" id="delete{{$item->id}}">
        <div class="modal-dialog">
          <div class="modal-content bg-danger">
            <div class="modal-header">
              <h5 class="modal-title">Delete Data</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Anda yakin akan menghapus "{{$item->sasaran}}" dari daftar?</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-outline pull-left text-white" data-dismiss="modal">Cancel</button>
              <a href="/admin/dpasubkegiatandelete/{{ $item->id }}" class="btn btn-outline">
              <button type="button" class="btn btn-outline text-white">Delete</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
    @endforeach

@endsection

