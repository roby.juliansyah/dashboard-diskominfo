<!DOCTYPE html>
<html lang="en">
@include('template.head')
<body id="body" class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">
    <div class="preloader flex-column justify-content-center align-items-center"> <!-- Preloader -->
      <img class="animation__shake" src="{{asset('template')}}/dist/img/DiskominfoJabar.png" alt="DiskominfoLogo" height="60">
    </div>

    @include('template.navbar')
    @include('template.sidebar')

    @yield('konten')
    @include('template.footer')
  </div> <!-- ./wrapper -->
  @include('template.script') 
  @yield('script')
</body>
</html>