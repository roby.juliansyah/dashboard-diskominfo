    <aside class="main-sidebar sidebar-dark-primary elevation-4"> <!-- Main Sidebar Container -->
      <a href="/" class="brand-link"> <!-- Brand Logo -->
        <img src="{{asset('template')}}/dist/img/DiskominfoJabar.png" alt="Diskominfo Logo" class="center" height="68">
        <span class="brand-text font-weight-light">&nbsp;</span>
      </a> 
      <div class="sidebar"> <!-- Sidebar -->
        <div class="user-panel mt-2 pb-2 mb-2 d-flex"> <!-- Sidebar user panel (optional) -->
          <div class="image">
            <img src="{{asset('template')}}/dist/img/Agile.png" alt="User Image">
          </div>
          <div class="info">
            <a href="#" class="d-block">{{auth()->user()->name}}</a>
          </div>
        </div>
        <div class="user-panel mt-2 pb-2 mb-2"> <!-- Sidebar user panel (optional) -->
          <ul class="nav nav-sidebar">
            <li class="nav-item">
              <a href="/" class="nav-link">
                <i class="fas fa-tachometer-alt nav-icon"></i>
                <p class="text-sm">Dashboard</p>
              </a>
            </li>
          </ul>
        </div>
        <nav class="mt-2"> <!-- Sidebar Menu -->
          
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item"> 
              <a href="#" class="nav-link @yield('sidebardpa')">
                <i class="nav-icon fas fa-folder"></i>
                <p>Data DPA<i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                @if( auth()->user()->id_seksi === null )
                  <li class="nav-item">
                    <a href="dpakegiatan" class="nav-link @yield('sidebardpakegiatan')">
                      <i class="fas fa-cubes nav-icon"></i>
                      <p class="text-sm">Data Kegiatan</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="dpakegiatanindikator" class="nav-link @yield('sidebardpakegiatanindikator')">
                      <i class="fas fa-cube nav-icon"></i>
                      <p class="text-sm">Data Indikator Kegiatan</p>
                    </a>
                  </li>
                @endif
                <li class="nav-item">
                  <a href="dpasubkegiatan" class="nav-link @yield('sidebardpasubkegiatan')">
                    <i class="fas fa-cubes nav-icon"></i>
                    <p class="text-sm">Data Sub Kegiatan</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="dpasubkegiatanindikator" class="nav-link @yield('sidebardpasubkegiatanindikator')">
                    <i class="fas fa-cube nav-icon"></i>
                    <p class="text-sm">Data Indikator Sub Kegiatan</p>
                  </a>
                </li>               
              </ul>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link @yield('sidebardata')">
                <i class="nav-icon fas fa-database"></i>
                <p>Input Data Periodik<i class="right fas fa-angle-left"></i></p>
              </a>
              <ul class="nav nav-treeview">
                @if( auth()->user()->id_seksi === null )
                  <li class="nav-item">
                    <a href="#" class="nav-link @yield('sidebardatakegiatan')">
                      <i class="fas fa-cubes nav-icon"></i>
                      <p class="text-sm">Kegiatan<i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                      <li class="nav-item">
                        <a href="datakegiatansasaran" class="nav-link @yield('sidebardatakegiatansasaran')">
                          <i class="fas fa-chart-area nav-icon"></i>
                          <p class="text-xs">Capaian Sasaran</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="#" class="nav-link disabled @yield('sidebardatakegiatanserapan')">
                          <i class="fas fa-chart-line nav-icon"></i>
                          <p class="text-xs">Serapan Anggaran<span class="right badge badge-danger">Disabled</span></p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="#" class="nav-link disabled @yield('sidebardatakegiatanfisik')">
                          <i class="fas fa-chart-bar nav-icon"></i>
                          <p class="text-xs">Serapan Fisik<span class="right badge badge-danger">Disabled</span></p>
                        </a>
                      </li>
                    </ul>
                  </li>
                @endif
                <li class="nav-item">
                  <a href="#" class="nav-link @yield('sidebardatasubkegiatan')">
                    <i class="fas fa-cubes nav-icon"></i>
                    <p class="text-sm">Sub Kegiatan<i class="right fas fa-angle-left"></i></p>
                  </a>
                  <ul class="nav nav-treeview">
                    <li class="nav-item">
                      <a href="datasubkegiatansasaran" class="nav-link @yield('sidebardatasubkegiatansasaran')">
                        <i class="fas fa-chart-area nav-icon"></i>
                        <p class="text-xs">Capaian Sasaran</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="datasubkegiatanserapan" class="nav-link @yield('sidebardatasubkegiatanserapan')">
                        <i class="fas fa-chart-line nav-icon"></i>
                        <p class="text-xs">Serapan Anggaran</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="datasubkegiatanfisik" class="nav-link @yield('sidebardatasubkegiatanfisik')">
                        <i class="fas fa-chart-bar nav-icon"></i>
                        <p class="text-xs">Serapan Fisik</p>
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="nav-item">
              <a href="datalayanan" class="nav-link @yield('sidebardatalayanan')">
                <i class="nav-icon fas fa-info"></i>
                <p>Data Layanan<span class="right badge badge-danger">Dev</span></p>
              </a>
            </li>
            @if( auth()->user()->id_bidang === null )
              <li class="nav-item">
                <a href="#" class="nav-link @yield('sidebarmaster')">
                  <i class="nav-icon fas fa-folder"></i>
                  <p>Data Master<i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="masterbidang" class="nav-link @yield('sidebarmasterbidang')">
                      <i class="fas fa-cubes nav-icon"></i>
                      <p class="text-sm">Data Bidang/UPTD</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="masterseksi" class="nav-link @yield('sidebarmasterseksi')">
                      <i class="fas fa-cube nav-icon"></i>
                      <p class="text-sm">Data Seksi/Subbagian</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="masteruser" class="nav-link @yield('sidebarmasteruser')">
                      <i class="fas fa-users nav-icon"></i>
                      <p class="text-sm">Data User</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link @yield('sidebarmasterpegawai')">
                      <i class="fas fa-users nav-icon"></i>
                      <p class="text-sm">Data Pegawai<span class="right badge badge-danger">Dev</span></p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="mastersatuan" class="nav-link @yield('sidebarmastersatuan')">
                      <i class="fas fa-square nav-icon"></i>
                      <p class="text-sm">Data Satuan<span class="right badge badge-danger">Dev</span></p>
                    </a>
                  </li>               
                </ul>
              </li>
            @endif
            <li class="nav-item">
                <p></p>
            </li>
            <li class="nav-item">
              <form id="logout-form" action="{{ route('logout') }}" method="POST">
                @csrf
                <button type="submit" class="nav-link btn-danger">
                <i class="nav-icon fas fa-sign-out-alt text-light"></i>
                  <p class="text-light">Sign Out</p>
                </button>
              </form>
            </li>
          </ul>
          
        </nav>
      </div>
    </aside>