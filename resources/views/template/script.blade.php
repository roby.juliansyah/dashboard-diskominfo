<script src="{{asset('template')}}/plugins/jquery/jquery.min.js"></script> <!-- jQuery -->
<script src="{{asset('template')}}/plugins/bootstrap/js/bootstrap.min.js"></script><!-- Bootstrap 4 -->
<script src="{{asset('template')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script><!-- Bootstrap 4 -->
<script src="{{asset('template')}}/plugins/jquery-ui/jquery-ui.min.js"></script> <!-- jQuery UI 1.11.4 -->
<script src="{{asset('template')}}/plugins/chart.js/Chart.min.js"></script><!-- ChartJS -->
<script src="{{asset('template')}}/plugins/sparklines/sparkline.js"></script><!-- Sparkline -->
<script src="{{asset('template')}}/plugins/jqvmap/jquery.vmap.min.js"></script><!-- JQVMap -->
<script src="{{asset('template')}}/plugins/jqvmap/maps/jquery.vmap.usa.js"></script><!-- jQuery Knob Chart -->
<script src="{{asset('template')}}/plugins/jquery-knob/jquery.knob.min.js"></script><!-- daterangepicker -->
<script src="{{asset('template')}}/plugins/moment/moment.min.js"></script>
<script src="{{asset('template')}}/plugins/daterangepicker/daterangepicker.js"></script>
<script src="{{asset('template')}}/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script><!-- Tempusdominus Bootstrap 4 -->
<script src="{{asset('template')}}/plugins/summernote/summernote-bs4.min.js"></script><!-- Summernote -->
<script src="{{asset('template')}}/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script><!-- overlayScrollbars -->
<script src="{{asset('template')}}/plugins/fullcalendar/main.js"></script>
<script src="{{asset('template')}}/dist/js/adminlte.js"></script><!-- AdminLTE App -->
<script src="{{asset('template')}}/dist/js/adminlte.min.js"></script><!-- AdminLTE App -->
<script src="{{asset('template')}}/dist/js/demo.js"></script><!-- AdminLTE for demo purposes -->
<script src="{{asset('template')}}/dist/js/pages/dashboard.js"></script><!-- AdminLTE dashboard demo (This is only for demo purposes) -->

<script src="{{asset('template')}}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{asset('template')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{asset('template')}}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{asset('template')}}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="{{asset('template')}}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{asset('template')}}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="{{asset('template')}}/plugins/jszip/jszip.min.js"></script>
<script src="{{asset('template')}}/plugins/pdfmake/pdfmake.min.js"></script>
<script src="{{asset('template')}}/plugins/pdfmake/vfs_fonts.js"></script>
<script src="{{asset('template')}}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="{{asset('template')}}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="{{asset('template')}}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
  
<script>$.widget.bridge('uibutton', $.ui.button)</script> <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

	<script>
		$(document).ready(function() {
		window.setTimeout(function() {
			$(".alert").fadeTo(500, 0).slideUp(500, function(){
			$(this).remove();
			});
		}, 5000);
		});
  </script>
  
  <script>
	$(function () {
		$("#example1").DataTable({
		"responsive": true, "lengthChange": true, "autoWidth": true,
		"buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
		}).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
		$('#example2').DataTable({
		"paging": true,
		"lengthChange": false,
		"searching": false,
		"ordering": true,
		"info": true,
		"autoWidth": true,
		"responsive": true,
		});
	});
  </script>

  <script>
    $('#nama_bidang').change(function(){
      var id_bidang = $(this).val();
      if(id_bidang){
        $.ajax({
          type:"GET",
          url:"/loadseksi?id_bidang="+id_bidang,
          dataType: 'JSON',
          success:function(res){               
            if(res){
              $("#nama_seksi").prop( "disabled", false );
              $("#nama_seksi").empty();
              //$("#nama_divisi").empty();
              $("#nama_seksi").append('<option> - Pilih Seksi - </option>');
              //$("#nama_divisi").append('<option value=""> - Pilih - </option>');
              $.each(res,function(nama,id){
                $("#nama_seksi").append('<option value="'+id+'">'+nama+'</option>');
              });
            }else{
              $("#nama_seksi").prop( "disabled", true );
              $("#nama_seksi").empty();
              $("#nama_seksi").append('<option> - Pilih Seksi - </option>');
              //$("#nama_divisi").empty();
              //$("#nama_divisi").append('<option> - Pilih - </option>');
              
            }
          }
        });
      }else{
        $("#nama_seksi").prop( "disabled", true );
        $("#nama_seksi").empty();
        $("#nama_seksi").append('<option> - Pilih - </option>');
        //$("#nama_divisi").empty();
        //$("#nama_divisi").append('<option> - Pilih - </option>');
      }      
    });
  </script>

<script>
    $('#nama_bidang').change(function(){
      var id_bidang = $(this).val();
      if(id_bidang){
        $.ajax({
          type:"GET",
          url:"/loadkegiatan?id_bidang="+id_bidang,
          dataType: 'JSON',
          success:function(res){               
            if(res){
              $("#nama_sub_kegiatan").prop( "disabled", true );
              $("#nama_sub_kegiatan").empty();
              $("#nama_sub_kegiatan").append('<option> - Pilih - </option>');
              $("#sasaran_kegiatan_indikator").prop( "disabled", true );
              $("#sasaran_kegiatan_indikator").empty();
              $("#sasaran_kegiatan_indikator").append('<option> - Pilih - </option>');
              $("#sasaran_sub_kegiatan_indikator").prop( "disabled", true );
              $("#sasaran_sub_kegiatan_indikator").empty();
              $("#sasaran_sub_kegiatan_indikator").append('<option> - Pilih - </option>');
              $("#target_kegiatan_indikator").prop( "disabled", true );
              $("#target_kegiatan_indikator").empty();
              $("#target_kegiatan_indikator").append('<input name="target" class="form-control" value="0" readonly>');
              $("#kegiatan_indikator_lalu").empty();
			  $("#kegiatan_indikator_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');
              $("#fisik_kegiatan_lalu").empty();0
			  $("#fisik_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');
              $("#serapan_kegiatan_lalu").empty();
			  $("#serapan_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="@currency(0)" readonly>');              
              $("#anggaran_kegiatan").prop( "disabled", true );
              $("#anggaran_kegiatan").empty();
              $("#anggaran_kegiatan").append('<input name="anggaran" class="form-control" value="@currency(0)" readonly>');
              $("#target_sub_kegiatan_indikator").prop( "disabled", true );
              $("#target_sub_kegiatan_indikator").empty();
              $("#target_sub_kegiatan_indikator").append('<input name="target" class="form-control" value="0" readonly>');
              $("#anggaran_sub_kegiatan").prop( "disabled", true );
              $("#anggaran_sub_kegiatan").empty();
              $("#anggaran_sub_kegiatan").append('<input name="anggaran" class="form-control" value="@currency(0)" readonly>');
			  $("#sub_kegiatan_indikator_lalu").empty();
			  $("#sub_kegiatan_indikator_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');
              $("#serapan_sub_kegiatan_lalu").empty();
			  $("#serapan_sub_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="@currency(0)" readonly>');
              $("#fisik_sub_kegiatan_lalu").empty();
			  $("#fisik_sub_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');
              $("#nama_kegiatan").prop( "disabled", false );
              $("#nama_kegiatan").empty();
              $("#nama_kegiatan").append('<option> - Pilih Kegiatan - </option>');
              $.each(res,function(nama,id){
                $("#nama_kegiatan").append('<option value="'+id+'">'+nama+'</option>');
              });
            }else{
              $("#nama_sub_kegiatan").prop( "disabled", true );
              $("#nama_sub_kegiatan").empty();
              $("#nama_sub_kegiatan").append('<option> - Pilih - </option>');
              $("#sasaran_kegiatan_indikator").prop( "disabled", true );
              $("#sasaran_kegiatan_indikator").empty();
              $("#sasaran_kegiatan_indikator").append('<option> - Pilih - </option>');
              $("#sasaran_sub_kegiatan_indikator").prop( "disabled", true );
              $("#sasaran_sub_kegiatan_indikator").empty();
              $("#sasaran_sub_kegiatan_indikator").append('<option> - Pilih - </option>');
              $("#target_kegiatan_indikator").prop( "disabled", true );
              $("#target_kegiatan_indikator").empty();
              $("#target_kegiatan_indikator").append('<input name="target" class="form-control" value="0" readonly>');
              $("#anggaran_kegiatan").prop( "disabled", true );
              $("#anggaran_kegiatan").empty();
              $("#anggaran_kegiatan").append('<input name="anggaran" class="form-control" value="@currency(0)" readonly>');
              $("#kegiatan_indikator_lalu").empty();
			  $("#kegiatan_indikator_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');
              $("#fisik_kegiatan_lalu").empty();
			  $("#fisik_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');
              $("#serapan_kegiatan_lalu").empty();
			  $("#serapan_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="@currency(0)" readonly>');              
              $("#target_sub_kegiatan_indikator").prop( "disabled", true );
              $("#target_sub_kegiatan_indikator").empty();
              $("#target_sub_kegiatan_indikator").append('<input name="target" class="form-control" value="0" readonly>');
              $("#anggaran_sub_kegiatan").prop( "disabled", true );
              $("#anggaran_sub_kegiatan").empty();
              $("#anggaran_sub_kegiatan").append('<input name="anggaran" class="form-control" value="@currency(0)" readonly>');
			  $("#sub_kegiatan_indikator_lalu").empty();
			  $("#sub_kegiatan_indikator_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');
              $("#serapan_sub_kegiatan_lalu").empty();
			  $("#serapan_sub_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="@currency(0)" readonly>');
              $("#fisik_sub_kegiatan_lalu").empty();
			  $("#fisik_sub_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');
              $("#nama_kegiatan").prop( "disabled", true );
              $("#nama_kegiatan").empty();
              $("#nama_kegiatan").append('<option> - Pilih Kegiatan - </option>');              
            }
          }
        });
      }else{
        $("#nama_sub_kegiatan").prop( "disabled", true );
        $("#nama_sub_kegiatan").empty();
        $("#nama_sub_kegiatan").append('<option> - Pilih - </option>');
        $("#sasaran_kegiatan_indikator").prop( "disabled", true );
        $("#sasaran_kegiatan_indikator").empty();
        $("#sasaran_kegiatan_indikator").append('<option> - Pilih - </option>');
        $("#sasaran_sub_kegiatan_indikator").prop( "disabled", true );
        $("#sasaran_sub_kegiatan_indikator").empty();
        $("#sasaran_sub_kegiatan_indikator").append('<option> - Pilih - </option>');
        $("#target_kegiatan_indikator").prop( "disabled", true );
        $("#target_kegiatan_indikator").empty();
        $("#target_kegiatan_indikator").append('<input name="target" class="form-control" value="0" readonly>');
        $("#kegiatan_indikator_lalu").empty();
		$("#kegiatan_indikator_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');
        $("#fisik_kegiatan_lalu").empty();
		$("#fisik_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');
        $("#serapan_kegiatan_lalu").empty();
		$("#serapan_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="@currency(0)" readonly>');              
        $("#anggaran_kegiatan").prop( "disabled", true );
        $("#anggaran_kegiatan").empty();
        $("#anggaran_kegiatan").append('<input name="anggaran" class="form-control" value="@currency(0)" readonly>');
        $("#target_sub_kegiatan_indikator").prop( "disabled", true );
        $("#target_sub_kegiatan_indikator").empty();
        $("#target_sub_kegiatan_indikator").append('<input name="target" class="form-control" value="0" readonly>');
        $("#anggaran_sub_kegiatan").prop( "disabled", true );
        $("#anggaran_sub_kegiatan").empty();
        $("#anggaran_sub_kegiatan").append('<input name="anggaran" class="form-control" value="@currency(0)" readonly>');
		$("#sub_kegiatan_indikator_lalu").empty();
		$("#sub_kegiatan_indikator_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');
        $("#serapan_sub_kegiatan_lalu").empty();
		$("#serapan_sub_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="@currency(0)" readonly>');
        $("#fisik_sub_kegiatan_lalu").empty();
		$("#fisik_sub_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');
        $("#nama_kegiatan").prop( "disabled", true );
        $("#nama_kegiatan").empty();
        $("#nama_kegiatan").append('<option> - Pilih - </option>');
      }      
    });
  </script>

  <script>
    $('#nama_kegiatan').change(function(){
      var id_kegiatan = $(this).val();
      if(id_kegiatan){
        $.ajax({
          type:"GET",
          url:"/loadkegiatanindikator?id_kegiatan="+id_kegiatan,
          dataType: 'JSON',
          success:function(res){               
            if(res){
              $("#target_kegiatan_indikator").empty();
			  $("#target_kegiatan_indikator").append('<input name="target" class="form-control" value="0" readonly>');
              $("#kegiatan_indikator_lalu").empty();
			  $("#kegiatan_indikator_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');
              $("#fisik_kegiatan_lalu").empty();
			  $("#fisik_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');
              $("#serapan_kegiatan_lalu").empty();
			  $("#serapan_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="@currency(0)" readonly>');              
              $("#sasaran_kegiatan_indikator").prop( "disabled", false );
              $("#sasaran_kegiatan_indikator").empty();
              $("#sasaran_kegiatan_indikator").append('<option> - Pilih Sasaran Kegiatan - </option>');
              $.each(res,function(nama,id){
                $("#sasaran_kegiatan_indikator").append('<option value="'+id+'">'+nama+'</option>');
              });
            }else{
              $("#target_kegiatan_indikator").empty();
			  $("#target_kegiatan_indikator").append('<input name="target" class="form-control" value="0" readonly>');
              $("#kegiatan_indikator_lalu").empty();
			  $("#kegiatan_indikator_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');
              $("#fisik_kegiatan_lalu").empty();
			  $("#fisik_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');
              $("#serapan_kegiatan_lalu").empty();
			  $("#serapan_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="@currency(0)" readonly>');              
              $("#sasaran_kegiatan_indikator").prop( "disabled", true );
              $("#sasaran_kegiatan_indikator").empty();
              $("#sasaran_kegiatan_indikator").append('<option> - Pilih Sasaran Kegiatan - </option>');              
            }
          }
        });
      }else{
        $("#target_kegiatan_indikator").empty();
		$("#target_kegiatan_indikator").append('<input name="target" class="form-control" value="0" readonly>');
        $("#kegiatan_indikator_lalu").empty();
		$("#kegiatan_indikator_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');
        $("#fisik_kegiatan_lalu").empty();
		$("#fisik_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');
        $("#serapan_kegiatan_lalu").empty();
		$("#serapan_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="@currency(0)" readonly>');              
        $("#sasaran_kegiatan_indikator").prop( "disabled", true );
        $("#sasaran_kegiatan_indikator").empty();
        $("#sasaran_kegiatan_indikator").append('<option> - Pilih - </option>');
      }      
    });
  </script>

  <script>
    $('#nama_kegiatan').change(function(){
      var id_kegiatan = $(this).val();
      if(id_kegiatan){
        $.ajax({
          type:"GET",
          url:"/loadsubkegiatan?id_kegiatan="+id_kegiatan,
          dataType: 'JSON',
          success:function(res){               
            if(res){
              $("#anggaran_sub_kegiatan").empty();
			  $("#anggaran_sub_kegiatan").append('<input name="anggaran" class="form-control" value="@currency(0)" readonly>');
              $("#target_sub_kegiatan_indikator").empty();
			  $("#target_sub_kegiatan_indikator").append('<input name="target" class="form-control" value="0" readonly>');
              $("#sasaran_sub_kegiatan_indikator").prop( "disabled", true );
			  $("#sasaran_sub_kegiatan_indikator").empty();
			  $("#sasaran_sub_kegiatan_indikator").append('<option> - Pilih - </option>');
              $("#nama_sub_kegiatan").prop( "disabled", false );
              $("#nama_sub_kegiatan").empty();
              $("#nama_sub_kegiatan").append('<option> - Pilih Sub Kegiatan - </option>');
			  $("#sub_kegiatan_indikator_lalu").empty();
			  $("#sub_kegiatan_indikator_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');
              $("#serapan_sub_kegiatan_lalu").empty();
			  $("#serapan_sub_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="@currency(0)" readonly>');
              $("#fisik_sub_kegiatan_lalu").empty();
			  $("#fisik_sub_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');
              $.each(res,function(nama,id){
                $("#nama_sub_kegiatan").append('<option value="'+id+'">'+nama+'</option>');
              });
            }else{
              $("#anggaran_sub_kegiatan").empty();
			  $("#anggaran_sub_kegiatan").append('<input name="anggaran" class="form-control" value="@currency(0)" readonly>');
              $("#target_sub_kegiatan_indikator").empty();
			  $("#target_sub_kegiatan_indikator").append('<input name="target" class="form-control" value="0" readonly>');
              $("#sasaran_sub_kegiatan_indikator").prop( "disabled", true );
			  $("#sasaran_sub_kegiatan_indikator").empty();
			  $("#sasaran_sub_kegiatan_indikator").append('<option> - Pilih - </option>');
              $("#nama_sub_kegiatan").prop( "disabled", true );
              $("#nama_sub_kegiatan").empty();
              $("#nama_sub_kegiatan").append('<option> - Pilih Sub Kegiatan - </option>');
			  $("#sub_kegiatan_indikator_lalu").empty();
			  $("#sub_kegiatan_indikator_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');
              $("#serapan_sub_kegiatan_lalu").empty();
			  $("#serapan_sub_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="@currency(0)" readonly>');
              $("#fisik_sub_kegiatan_lalu").empty();
			  $("#fisik_sub_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');         
            }
          }
        });
      }else{
        $("#anggaran_sub_kegiatan").empty();
		$("#anggaran_sub_kegiatan").append('<input name="anggaran" class="form-control" value="@currency(0)" readonly>');
        $("#target_sub_kegiatan_indikator").empty();
		$("#target_sub_kegiatan_indikator").append('<input name="target" class="form-control" value="0" readonly>');
        $("#sasaran_sub_kegiatan_indikator").prop( "disabled", true );
		$("#sasaran_sub_kegiatan_indikator").empty();
        $("#sasaran_sub_kegiatan_indikator").append('<option> - Pilih - </option>');
        $("#nama_sub_kegiatan").prop( "disabled", true );
        $("#nama_sub_kegiatan").empty();
        $("#nama_sub_kegiatan").append('<option> - Pilih - </option>');
		$("#sub_kegiatan_indikator_lalu").empty();
	  	$("#sub_kegiatan_indikator_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');
        $("#serapan_sub_kegiatan_lalu").empty();
		$("#serapan_sub_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="@currency(0)" readonly>');
        $("#fisik_sub_kegiatan_lalu").empty();
		$("#fisik_sub_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');
      }      
    });
  </script>

  <script>
		$('#sasaran_kegiatan_indikator').change(function(){
			var id = $(this).val();
			if(id){
				$.ajax({
					type:"GET",
					url:"/loadtargetkegiatanindikator?id="+id,
					dataType: 'JSON',
					success:function(res){               
						if(res){
							$("#target_kegiatan_indikator").empty();
							$.each(res,function(satuan,target){
								$("#target_kegiatan_indikator").append('<input name="target" class="form-control" value="'+target+' '+satuan+'" readonly>');
							});
						}else{
							$("#target_kegiatan_indikator").empty();
							$("#target_kegiatan_indikator").append('<input name="target" class="form-control" value="0" readonly>');              
						}
					}
				});
			}else{
				$("#target_kegiatan_indikator").empty();
				$("#target_kegiatan_indikator").append('<input name="target" class="form-control" value="0" readonly>');
			}
		});
	</script>

  <script>
		$('#sasaran_kegiatan_indikator').change(function(){
      var id_indikator_kegiatan = $(this).val();
			if(id_indikator_kegiatan){
				$.ajax({
					type:"GET",
					url:"/loadkegiatanindikatorlalu?id_indikator_kegiatan="+id_indikator_kegiatan,
					dataType: 'JSON',
					success:function(res){               
						if(res){
							$("#kegiatan_indikator_lalu").empty();
							$.each(res,function(capaian,capaian_lalu){
								$("#kegiatan_indikator_lalu").append('<input name="capaian_lalu" class="form-control" value="'+capaian+'" readonly>');
							});
						}else{
							$("#kegiatan_indikator_lalu").empty();
							$("#kegiatan_indikator_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');              
						}
					}
				});
			}else{
				$("#kegiatan_indikator_lalu").empty();
				$("#kegiatan_indikator_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');
			}
		});
	</script>

  <script>
		$('#nama_kegiatan').change(function(){
			var id = $(this).val();
			if(id){
				$.ajax({
					type:"GET",
					url:"/loadanggarankegiatan?id="+id,
					dataType: 'JSON',
					success:function(res){               
						if(res){
							$("#anggaran_kegiatan").empty();
							$.each(res,function(nama,anggaran){
								$("#anggaran_kegiatan").append('<input name="anggaran" class="form-control" value="Rp. '+anggaran.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+'" readonly>');
							});
						}else{
							$("#anggaran_kegiatan").empty();
							$("#anggaran_kegiatan").append('<input name="anggaran" class="form-control" value="@currency(0)" readonly>');              
						}
					}
				});
			}else{
				$("#anggaran_kegiatan").empty();
				$("#anggaran_kegiatan").append('<input name="anggaran" class="form-control" value="@currency(0)" readonly>');
			}
		});
	</script>

  <script>
		$('#nama_kegiatan').change(function(){
			var id_kegiatan = $(this).val();
			if(id_kegiatan){
				$.ajax({
					type:"GET",
					url:"/loadfisikkegiatanlalu?id_kegiatan="+id_kegiatan,
					dataType: 'JSON',
					success:function(res){               
						if(res){
							$("#fisik_kegiatan_lalu").empty();
							$.each(res,function(capaian,capaian_lalu){
                				$("#fisik_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="'+capaian+'" readonly>');
							});
						}else{
							$("#fisik_kegiatan_lalu").empty();
							$("#fisik_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');              
						}
					}
				});
			}else{
				$("#fisik_kegiatan_lalu").empty();
				$("#fisik_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');
			}
		});
	</script>

  <script>
		$('#nama_kegiatan').change(function(){
			var id_kegiatan = $(this).val();
			if(id_kegiatan){
				$.ajax({
					type:"GET",
					url:"/loadserapankegiatanlalu?id_kegiatan="+id_kegiatan,
					dataType: 'JSON',
					success:function(res){               
						if(res){
							$("#serapan_kegiatan_lalu").empty();
							$.each(res,function(capaian,capaian_lalu){
                				$("#serapan_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="Rp. '+capaian.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+'" readonly>');
							});
						}else{
							$("#serapan_kegiatan_lalu").empty();
							$("#serapan_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="@currency(0)" readonly>');              
						}
					}
				});
			}else{
				$("#serapan_kegiatan_lalu").empty();
				$("#serapan_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="@currency(0)" readonly>');
			}
		});
	</script>

  <script>
		$('#nama_sub_kegiatan').change(function(){
      var id_sub_kegiatan = $(this).val();
			if(id_sub_kegiatan){
				$.ajax({
					type:"GET",
					url:"/loadsubkegiatanindikator?id_sub_kegiatan="+id_sub_kegiatan,
					dataType: 'JSON',
					success:function(res){               
						if(res){
							$("#target_sub_kegiatan_indikator").empty();
							$("#target_sub_kegiatan_indikator").append('<input name="target" class="form-control" value="0" readonly>');
							$("#sub_kegiatan_indikator_lalu").empty();
							$("#sub_kegiatan_indikator_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');
              				$("#sasaran_sub_kegiatan_indikator").prop( "disabled", false );
							$("#sasaran_sub_kegiatan_indikator").empty();
							$("#sasaran_sub_kegiatan_indikator").append('<option> - Pilih Indikator - </option>');
							$.each(res,function(nama,id){
								$("#sasaran_sub_kegiatan_indikator").append('<option value="'+id+'">'+nama+'</option>');
							});
						}else{
							$("#target_sub_kegiatan_indikator").empty();
							$("#target_sub_kegiatan_indikator").append('<input name="target" class="form-control" value="0" readonly>');
							$("#sub_kegiatan_indikator_lalu").empty();
							$("#sub_kegiatan_indikator_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');
              				$("#sasaran_sub_kegiatan_indikator").prop( "disabled", true );
							$("#sasaran_sub_kegiatan_indikator").empty();
							$("#sasaran_sub_kegiatan_indikator").append('<option> - Pilih Indikator - </option>');              
						}
					}
				});
			}else{
				$("#target_sub_kegiatan_indikator").empty();
				$("#target_sub_kegiatan_indikator").append('<input name="target" class="form-control" value="0" readonly>');
				$("#sub_kegiatan_indikator_lalu").empty();
				$("#sub_kegiatan_indikator_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');
        		$("#sasaran_sub_kegiatan_indikator").prop( "disabled", true );
				$("#sasaran_sub_kegiatan_indikator").empty();
				$("#sasaran_sub_kegiatan_indikator").append('<option> - Pilih - </option>');
			}
		});
	</script>

  <script>
		$('#sasaran_sub_kegiatan_indikator').change(function(){
			var id = $(this).val();
			if(id){
				$.ajax({
					type:"GET",
					url:"/loadtargetsubkegiatanindikator?id="+id,
					dataType: 'JSON',
					success:function(res){               
						if(res){
							$("#target_sub_kegiatan_indikator").empty();
							$.each(res,function(satuan,target){
								$("#target_sub_kegiatan_indikator").append('<input name="target" class="form-control" value="'+target+' '+satuan+'" readonly>');
							});
						}else{
							$("#target_sub_kegiatan_indikator").empty();
							$("#target_sub_kegiatan_indikator").append('<input name="target" class="form-control" value="0" readonly>');              
						}
					}
				});
			}else{
				$("#target_sub_kegiatan_indikator").empty();
				$("#target_sub_kegiatan_indikator").append('<input name="target" class="form-control" value="0" readonly>');
			}
		});
	</script>
  
  <script>
		$('#nama_sub_kegiatan').change(function(){
			var id = $(this).val();
			if(id){
				$.ajax({
					type:"GET",
					url:"/loadanggaransubkegiatan?id="+id,
					dataType: 'JSON',
					success:function(res){               
						if(res){
							$("#anggaran_sub_kegiatan").empty();
							$.each(res,function(nama,anggaran){
								$("#anggaran_sub_kegiatan").append('<input name="anggaran" class="form-control" value="Rp. '+anggaran.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+'" readonly>');
							});
						}else{
							$("#anggaran_sub_kegiatan").empty();
							$("#anggaran_sub_kegiatan").append('<input name="anggaran" class="form-control" value="@currency(0)" readonly>');              
						}
					}
				});
			}else{
				$("#anggaran_sub_kegiatan").empty();
				$("#anggaran_sub_kegiatan").append('<input name="anggaran" class="form-control" value="@currency(0)" readonly>');
			}
		});
	</script>

  <script>
    $('#nama_kegiatan_by_seksi').change(function(){
      var id_kegiatan = $(this).val();
      if(id_kegiatan){
        $.ajax({
          type:"GET",
          url:"/loadsubkegiatanbyseksi?id_kegiatan="+id_kegiatan,
          dataType: 'JSON',
          success:function(res){               
            if(res){
              $("#nama_sub_kegiatan").prop( "disabled", false );
              $("#nama_sub_kegiatan").empty();
              $("#nama_sub_kegiatan").append('<option> - Pilih Sub Kegiatan - </option>');
              $.each(res,function(nama,id){
                $("#nama_sub_kegiatan").append('<option value="'+id+'">'+nama+'</option>');
              });
            }else{
              $("#nama_sub_kegiatan").prop( "disabled", true );
              $("#nama_sub_kegiatan").empty();
              $("#nama_sub_kegiatan").append('<option> - Pilih Sub Kegiatan - </option>');              
            }
          }
        });
      }else{
        $("#nama_sub_kegiatan").prop( "disabled", true );
        $("#nama_sub_kegiatan").empty();
        $("#nama_sub_kegiatan").append('<option> - Pilih - </option>');
      }      
    });
  	</script>

	<script>
		$('#sasaran_sub_kegiatan_indikator').change(function(){
      		var id_indikator_sub_kegiatan = $(this).val();
			if(id_indikator_sub_kegiatan){
				$.ajax({
					type:"GET",
					url:"/loadsubkegiatanindikatorlalu?id_indikator_sub_kegiatan="+id_indikator_sub_kegiatan,
					dataType: 'JSON',
					success:function(res){               
						if(res){
							$("#sub_kegiatan_indikator_lalu").empty();
							$.each(res,function(capaian,capaian_lalu){
								$("#sub_kegiatan_indikator_lalu").append('<input name="capaian_lalu" class="form-control" value="'+capaian+'" readonly>');
							});
						}else{
							$("#sub_kegiatan_indikator_lalu").empty();
							$("#sub_kegiatan_indikator_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');              
						}
					}
				});
			}else{
				$("#sub_kegiatan_indikator_lalu").empty();
				$("#sub_kegiatan_indikator_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');
			}
		});
	</script>
	
	<script>
		$('#nama_sub_kegiatan').change(function(){
			var id_sub_kegiatan = $(this).val();
			if(id_sub_kegiatan){
				$.ajax({
					type:"GET",
					url:"/loadserapansubkegiatanlalu?id_sub_kegiatan="+id_sub_kegiatan,
					dataType: 'JSON',
					success:function(res){               
						if(res){
							$("#serapan_sub_kegiatan_lalu").empty();
							$.each(res,function(capaian,capaian_lalu){
                				$("#serapan_sub_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="Rp. '+capaian.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+'" readonly>');
							});
						}else{
							$("#serapan_sub_kegiatan_lalu").empty();
							$("#serapan_sub_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="@currency(0)" readonly>');
						}
					}
				});
			}else{
				$("#serapan_sub_kegiatan_lalu").empty();
				$("#serapan_sub_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="@currency(0)" readonly>');
			}
		});
	</script>

<script>
		$('#nama_sub_kegiatan').change(function(){
			var id_sub_kegiatan = $(this).val();
			if(id_sub_kegiatan){
				$.ajax({
					type:"GET",
					url:"/loadfisiksubkegiatanlalu?id_sub_kegiatan="+id_sub_kegiatan,
					dataType: 'JSON',
					success:function(res){               
						if(res){
							$("#fisik_sub_kegiatan_lalu").empty();
							$.each(res,function(capaian,capaian_lalu){
                				$("#fisik_sub_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="'+capaian+'" readonly>');
							});
						}else{
							$("#fisik_sub_kegiatan_lalu").empty();
							$("#fisik_sub_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');              
						}
					}
				});
			}else{
				$("#fisik_sub_kegiatan_lalu").empty();
				$("#fisik_sub_kegiatan_lalu").append('<input name="capaian_lalu" class="form-control" value="0" readonly>');
			}
		});
	</script>

  <script> 
    function addClass() {
      var v = document.getElementById("body");
      v.classList.add("dark-mode");
    }
    function removeClass() {
      var v = document.getElementById("body");
      v.classList.remove("dark-mode");
    }
  </script>

  <script type="text/javascript">
		var rupiah = document.getElementById('rupiah');
		rupiah.addEventListener('keyup', function(e){
			// tambahkan 'Rp.' pada saat form di ketik
			// gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
			rupiah.value = formatRupiah(this.value, 'Rp. ');
		});
 
		/* Fungsi formatRupiah */
		function formatRupiah(angka, prefix){
			var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}
 
			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
		}
	</script>