          <div class="row">
            <div class="col-lg-4 col-6"> <!-- small box -->
              <div class="small-box bg-primary">
                <div class="inner">
                  <p>Anggaran</p>
                  @foreach ($anggarankegiatan as $anggaranitem)
                    <h3>Rp. {{ number_format($anggaranitem->jumlahanggaran/1000000000, 2, ',', '.') }} M,- </h3>
                  @endforeach
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
              </div>
            </div> <!-- ./col -->
            <div class="col-lg-4 col-6"> <!-- small box -->
              <div class="small-box bg-warning">
                <div class="inner">
                  <p>Realisasi Anggaran</p>
                  <h3>Rp. {{ number_format($realisasianggaran/1000000000, 2, ',', '.') }} M,- </h3>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
              </div>
            </div> <!-- ./col -->
            <div class="col-lg-4 col-6"> <!-- small box -->
              <div class="small-box">
                <div class="inner">
                  <p>Keterserapan Anggaran</p>
                  <h3>{{ number_format($realisasianggaran/$anggaranitem->jumlahanggaran*100, 2, ',', '.') }} %</h3>
                </div>
                <div class="icon">
                  <i class="ion ion-calculator"></i>
                </div> 
              </div>
            </div>
            <!--
            <div class="col-lg-3 col-6">
              <div class="small-box bg-success">
                <div class="inner">
                  <p>Realisasi Fisik</p>
                  <h3>{{$realisasifisik}} %</h3>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
              </div>
            </div>
            -->
          </div> <!-- /.row -->