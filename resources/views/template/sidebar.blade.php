    <aside class="main-sidebar sidebar-dark-primary elevation-4"> <!-- Main Sidebar Container -->
      <a href="/" class="brand-link"> <!-- Brand Logo -->
        <img src="{{asset('template')}}/dist/img/DiskominfoJabar.png" alt="Diskominfo Logo" class="center" height="68">
        <span class="brand-text font-weight-light">&nbsp;</span>
      </a> 
      <div class="sidebar"> <!-- Sidebar -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex"> <!-- Sidebar user panel (optional) -->
          <div class="image">
            <img src="{{asset('template')}}/dist/img/Agile.png" alt="User Image">
          </div>
          <div class="info">
            <a href="#" class="d-block">{{auth()->user()->name}}</a>
          </div>
        </div>
        <nav class="mt-2"> <!-- Sidebar Menu -->
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            @yield('sidebar')
            <!--
            <li class="nav-item">
              <a href="#" class="nav-link @yield('sidebarsekretariat')">
                <i class="nav-icon fas fa-users"></i>
                <p>Sekretariat<i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="#" class="nav-link @yield('sidebarperencanaan')">
                    <i class="fas fa-check-square nav-icon"></i>
                    <p class="text-sm">Perencanaan & Pelaporan</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="#" class="nav-link @yield('sidebarkeuangan')">
                    <i class="fas fa-balance-scale nav-icon"></i>
                    <p class="text-sm">Keuangan & Aset</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="#" class="nav-link @yield('sidebarkepegawaian')">
                    <i class="fas fa-sitemap nav-icon"></i>
                    <p class="text-sm">Kepegawaian & Umum</p>
                  </a>
                </li>
              </ul>
            </li> 
            <li class="nav-item">
              <a href="/egov" class="nav-link @yield('sidebaregov')">
                <i class="nav-icon fas fa-wifi"></i>
                <p>e-Government</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/aptika" class="nav-link @yield('sidebaraptika')">
                <i class="nav-icon fas fa-server"></i>
                <p>Aplikasi Informatika</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link @yield('sidebarikp')">
                <i class="nav-icon fas fa-share"></i>
                <p>IKP</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link @yield('sidebarsandikami')">
                <i class="nav-icon fas fa-user-secret"></i>
                <p>SandiKAMI</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link @yield('sidebarstatistik')">
                <i class="nav-icon fas fa-chart-line"></i>
                <p>Statistika</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link @yield('sidebaruptd')">
                <i class="nav-icon fas fa-mobile"></i>
                <p>UPTD PLDDIG</p>
              </a>
            </li>-->
          </ul>
        </nav><!-- /.sidebar-menu -->
        <div class="user-panel mt-2 pb-2 mb-2"> <!-- Sidebar user panel (optional) -->
        </div>
        <nav class="mt-2"> <!-- Sidebar Menu -->
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
              <a href="/admin/mainmenu" class="nav-link @yield('userman')">
                <i class="nav-icon fas fa-cog"></i>
                <p>Control Panel</p>
              </a>
            </li>
            <li class="nav-item">
                <p></p>
            </li>
            <li class="nav-item">
              <form id="logout-form" action="{{ route('logout') }}" method="POST">
                @csrf
                <button type="submit" class="nav-link btn-danger">
                <i class="nav-icon fas fa-sign-out-alt text-light"></i>
                  <p class="text-light">Sign Out</p>
                </button>
              </form>
            </li>
            
          </ul>
        </nav><!-- /.sidebar-menu -->
      </div><!-- /.sidebar -->
    </aside>