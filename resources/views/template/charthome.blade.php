        <div class="row"> <!-- Main row -->
            <section class="col-lg-4 connectedSortable"> <!-- BAR CHART -->
                <div class="card card-light">
                    <div class="card-header">
                        <h3 class="card-title">Performance Bidang</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart">
                            <canvas id="barChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                        </div>
                    </div> <!-- /.card-body -->
                </div> <!-- /.card -->
            </section>
            
            <section class="col-lg-4 connectedSortable">
                <div class="card card-ligt">
                    <div class="card-header">
                        <h3 class="card-title">Performance Dinas</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart d-flex justify-content-center">
                            <input type="text" class="knob " value="{{$performa}}" data-skin="tron" data-thickness="0.2"
                                data-angleArc="250" data-angleOffset="-125" data-width="250" data-height="250"
                                data-fgColor="{{$performacolor}}" readonly disabled=true>
                        </div>
                    </div>
                </div> 
            </section> 

            <section class="col-lg-4 connectedSortable"> <!-- BAR CHART -->
                <div class="card card-light">
                    <div class="card-header">
                        <h3 class="card-title">Data Kehadiran</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart">
                            <canvas id="pieChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                        </div>
                    </div> <!-- /.card-body -->
                </div> <!-- /.card -->
            </section>


            <section  class="col-lg-6 connectedSortable">
                <div class="card card-light">
                    <div class="card-header">
                        <h3 class="card-title">Kalender Kegiatan</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div id="external-events"></div>
                    <div class="card-body p-0">
                        <div id="calendar"></div>
                    </div>
                </div>                
            </section>

            <section  class="col-lg-6 connectedSortable">
                <div class="card card-light">
                    <div class="card-header">
                        <h3 class="card-title">Agenda Kepala Dinas</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                    </div>
                </div>                
            </section>
        </div>
