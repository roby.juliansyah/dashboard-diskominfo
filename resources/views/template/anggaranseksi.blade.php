        <div class="card card-success card-outline card-outline-tabs">
          <div class="card-header p-0 border-bottom-0">
            <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
              <?php
                $idtab=[
                  "custom-tabs-four-home",
                  "custom-tabs-four-profile",
                  "custom-tabs-four-settings"
                ];
                $i=0;
                $active=" active";
              ?>
              @foreach ($seksibybidang as $seksiitem)
                <li class="nav-item">
                  <a class="nav-link text-secondary{{$active}}" id="{{$idtab[$i]}}-tab" data-toggle="pill" href="#{{$idtab[$i]}}" role="tab" aria-controls="{{$idtab[$i]}}" aria-selected="true">{{$seksiitem->nama}}</a>
                </li>
                <?php
                  $i++;
                  $active = "";
                ?>
              @endforeach
            </ul>
          </div>
          <div class="card-body">
            <div class="tab-content" id="custom-tabs-four-tabContent">
              <?php
                $i=0;
                $active=" active";
              ?>
              @foreach ($seksibybidang as $seksiitem)
              <div class="tab-pane fade show{{$active}}" id="{{$idtab[$i]}}" role="tabpanel" aria-labelledby="{{$idtab[$i]}}-tab">
                <div class="row">
                  <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-primary">
                      <div class="inner">
                        <p>Anggaran</p>                        
                        <?php
                          $anggaran = 0;
                          $i++;
                          $active = "";
                        ?>
                        @foreach ($anggaransubkegiatanbybidang as $anggaranitem)
                          @if($anggaranitem->id_seksi===$seksiitem->id)
                          <?php $anggaran = $anggaran+$anggaranitem->anggaran; ?>
                          @endif
                        @endforeach
                        @if($anggaran>1000000000)
                          <h3>Rp. {{ number_format($anggaran/1000000000, 2, ',', '') }} M,- </h3>
                        @else
                          <h3>Rp. {{ number_format($anggaran/1000000, 2, ',', '') }} Jt,- </h3>
                        @endif
                      </div>
                      <div class="icon">
                        <i class="ion ion-bag"></i>
                      </div>
                    </div>
                  </div>
                  <!-- ./col -->
                  <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-warning">
                      <div class="inner">
                        <p>Realisasi</p>
                        <?php $datatempserapan = array(); ?>
                        @foreach ($datasubkegiatanserapan as $serapanitem)
                          @if($serapanitem->id_seksi === $seksiitem->id)
                            <?php
                              $datatempserapan[] = $serapanitem->capaian;
                            ?>
                            <!--<br>{{$serapanitem->capaian}}-->
                          @endif
                        @endforeach
                        <?php
                          if(count($datatempserapan)==0){
                            $serapan[$i-1]= 0;
                            $anggaran=0;
                            $serapananggaran = 100;
                          }else{
                            $serapan[$i-1] = array_sum($datatempserapan);
                            $datatempserapan = array();
                            $serapananggaran = $serapan[$i-1]/$anggaran*100;
                          }
                        ?>
                        @if($serapan[$i-1]>1000000000)
                          <h3>Rp. {{ number_format($serapan[$i-1]/1000000000, 2, ',', '') }} M,- </h3>
                        @else
                          <h3>Rp. {{ number_format($serapan[$i-1]/1000000, 2, ',', '') }} Jt,- </h3>
                        @endif
                      </div>
                      <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                      </div>
                    </div>
                  </div>
                  <!-- ./col -->
                  <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box">
                      <div class="inner">
                        <p>Keterserapan Anggaran</p>
                        <h3>{{number_format($serapananggaran, 2, ',', '.')}} %</h3>
                      </div>
                      <div class="icon">
                        <i class="ion ion-calculator"></i>
                      </div> 
                    </div>
                  </div>
                  <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-success">
                      <div class="inner">
                        <p>Realisasi Fisik</p>
                        @foreach ($datasubkegiatanfisik as $fisikitem)
                          @if($fisikitem->id_seksi === $seksiitem->id)
                            <?php
                              $datatempfisik[] = $fisikitem->capaian;
                            ?>
                            <!--<br> - {{$fisikitem->capaian}}-->
                          @endif
                        @endforeach
                        <?php
                          if(count($datatempfisik)==0){
                            $fisik[$i-1]= number_format(100,2,",",".");;; 
                          }else{
                            $fisik[$i-1] = number_format(array_sum($datatempfisik)/count($datatempfisik),2,",",".");;
                            $datatempfisik = array();
                          }
                        ?>
                        <h3>{{$fisik[$i-1]}} %</h3>
                      </div>
                      <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                      </div>
                    </div>
                  </div>
                  <!-- ./col -->
                </div>
                <!-- /.row -->
                <?php
                $performaseksi = number_format($data[$i-1],0,",",".");
                if($performaseksi < 60){
                  $seksicolor = "danger";
                }elseif($performaseksi < 80){
                  $seksicolor = "warning";
                }else{
                  $seksicolor = "success";
                }
                ?>
                <h5>Performa Seksi</h5>
                <div class="progress">
                  <div class="progress-bar bg-{{$seksicolor}} progress-bar-striped" role="progressbar" aria-valuenow="{{$performaseksi}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$performaseksi}}%">
                    {{$performaseksi}}%
                  </div>
                </div>
                <br>               
              </div>
              
              @endforeach
            </div>
          </div>
          <!-- /.card -->
        </div>